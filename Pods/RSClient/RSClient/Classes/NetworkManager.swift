//
//  NetworkManager.swift
//  RSClient
//
//  Created by YML on 02/03/16.
//  Copyright © 2016 YML. All rights reserved.
//

import Foundation
import UIKit

@objc public protocol  NetworkTasksDelegate  {
    @objc optional func isSSLNeeded(for request : URLRequest?) -> Bool
    @objc optional func performActionAfterLogout()
    @objc optional func getSSLPath() -> String
    
}

import Foundation
import UIKit
private let timeoutInterval = 60.0
private let domainName = "com.networkmanager.error"
private let concurrrentQueueIndentifier = "com.networkmanager.queue"

public typealias NetworkManagerCompletion = (_ response: JSON?, _ error: NetworkError?, _ task: NetworkTask) -> ()

var noNetworkError: NSError {
    let userInfo: [AnyHashable: Any] =
        [NSLocalizedDescriptionKey: NSLocalizedString("No Network",
                                                      value: "The Internet connection appears to be offline.",
                                                      comment: ""),
         ]
    return NSError(domain: "com.connectionManager.error", code: -1009, userInfo: userInfo as! [String : Any])
}

public enum HTTPMethod: String {
    
    case Get = "GET"
    case Put = "PUT"
    case Post = "POST"
    case Delete = "DELETE"
}

public enum NetworkManagerEncodingType: String {
    
    case URL = "multipart/form-data"
    case URLEncoded = "application/x-www-form-urlencoded"
    case Raw = "application/json"
}

public enum NetworkError: Error {
    // Can't connect to the server (maybe offline?)
    case connectionError(connectionError: NSError)
    // The server responded with a non 200 status code
    case serverError(statusCode: Int, message: String)
    // We got no data (0 bytes) back from the server
    case noDataError(statusCode: Int)
    // The server response can't be converted from JSON to a Dictionary
    case jsonSerializationError(statusCode: Int)
    
    var errorMessage: String {
        var message = ""
        
        switch self {
        case .connectionError(let connectionError):
            message = NSLocalizedString("\(connectionError.code)", tableName: "NetworkErrors", bundle: Bundle.main, value: "", comment: "")
        case .serverError(_, let messageString):
            message = messageString
            
        case .noDataError:
            message = "Sorry, server didn't respond with sufficient data."
        case .jsonSerializationError(_):
            message = "Sorry, we couldn't interpret server's response."
        }
        
        if message.characters.count == 0 { message = "Unknown error occured." }
        
        return message
    }
    
    var errorDomain: String {
        var domain = domainName
        
        switch self {
        case .connectionError(_):
            domain = "Connection Error"
        case .serverError(_,_):
            domain = "Server Error"
        case .noDataError:
            domain = "No Data"
        case .jsonSerializationError(_):
            domain = "Parse Error"
        }
        
        return domain
    }
    
    var code: Int {
        var errorCode: Int = 0
        
        switch self {
        case .connectionError(let connectionError):
            errorCode = connectionError.code
        case .serverError(let statusCode, _):
            errorCode = statusCode
        case .noDataError(let statusCode):
            errorCode = statusCode
        case .jsonSerializationError(let statusCode):
            errorCode = statusCode
        }
        
        return errorCode
    }
    
    var error: NSError {
        switch self {
        case .serverError(_,_):
            return NSError(domain: domainName, code: code)
            
        case .connectionError(_), .jsonSerializationError(_), .noDataError:
            return NSError(domain: domainName, code: code, userInfo: [NSLocalizedDescriptionKey: errorMessage])
        }
    }
}

public final class NetworkManager {
    
    static let queue = DispatchQueue(label:concurrrentQueueIndentifier, attributes: [])
    
    class public func requestForURL(_ urlString: String, method: HTTPMethod, params: [String: Any]?, headers: [String: String]?, encoding: NetworkManagerEncodingType = .Raw) -> NSMutableURLRequest {
        
        
        guard let escapedURLString = urlString.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed),
            let url = URL(string:escapedURLString) else {
                return NSMutableURLRequest()
        }
        
        let request = NSMutableURLRequest(url: url, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: timeoutInterval)
        URLCache.shared.removeCachedResponse(for: request as URLRequest)
        request.httpMethod = method.rawValue
        if let headers = headers {
            for (field, value) in headers {
                request.setValue(value, forHTTPHeaderField: field)
            }
        }
        switch (encoding) {
        case .URLEncoded:
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            break;
            //            if let params = params, params.count > 0 {
            //                var paramString = ""
            //                for (key, value) in params {
            //                    paramString += key + "=" + (value as! String)
            //
            //                    if Array(params.keys)[params.count - 1] != key {
            //                        paramString += "&"
            //                    }
            //                }
            //                let postData = paramString.data(using: String.Encoding.ascii, allowLossyConversion: true)
            //                request.httpBody = postData
            //                if let headers = headers {
            //                    for (field, value) in headers {
            //                        request.setValue(value, forHTTPHeaderField: field)
            //                    }
            //                }
            //            }
            
        case .Raw:
            if let params = params, params.count > 0 {
                
                request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
            }
            
        case .URL:
            var image = UIImage()
            var name = String()
            var key = String()
            if let params = params, params.count > 0 {
                image = params["image"] as! UIImage
                name = params["imageName"] as! String
                key = params["keyName"] as! String
            }
            let boundary = "---------------------------14737809831466499882746641449"
            let contentType = "multipart/form-data; boundary=\(boundary)"
            request.setValue(contentType, forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue("filename=\(name)", forHTTPHeaderField: "Content-Disposition")
            let body = NSMutableData()
            let data = UIImageJPEGRepresentation(image, 0.5)
            // print("size of images in byte \(data!.count)")
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(name)\"\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Type: image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append(data!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
            request.httpBody = body as Data
            
        }
        
        return request
    }
    
    class public func request(_ request: URLRequest) -> NetworkTask {
        let networkTask = NetworkTask(request: request, queue: queue)
        networkTask.networkTaskDelegate = networkTask as? NetworkTasksDelegate
        return networkTask
    }
}

public final class NetworkTask: NSObject {
    
    fileprivate var completionHandler: NetworkManagerCompletion?
    fileprivate var request : URLRequest?
    var networkTaskDelegate : NetworkTasksDelegate?
    convenience init(request: URLRequest, queue: DispatchQueue? = nil) {
        self.init()
        self.request = request
        let concurrentQueue = DispatchQueue(label: concurrrentQueueIndentifier, attributes: .concurrent)
        concurrentQueue.async {
            self.request(request)
        }
    }
    // MARK: - Public Methods
    
    func request(_ request: URLRequest) {
        
        // Check for internet connection
        if Reachability()?.isReachable == false {
            let noNetworkErrorType = NetworkError.connectionError(connectionError: noNetworkError)
            
            // Do Nothing if no completion block
            guard let completion = self.completionHandler else {
                return
            }
            return completion(nil, noNetworkErrorType, self)
        }
        
        let configuration = URLSessionConfiguration.default
        let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        
        let task = session.dataTask(with: request) { (data, response, error) in
            //            NetworkTask.logResponse(response, data: data, error: error as NSError?)
            
            // Do Nothing if no completion block
            guard let completion = self.completionHandler else {
                return
            }
            
            // Check for server error
            if let error = error {
                return completion(nil, NetworkError.connectionError(connectionError: error as NSError), self)
            }
            
            let httpResponse = response as! HTTPURLResponse
            
            // Parse Data to JSON Object
            
            guard let responseData = data, responseData.count > 0 else {
                return completion(nil, NetworkError.noDataError(statusCode: httpResponse.statusCode), self)
            }
            
            
            let responseObject = JSON(data: responseData)
            return completion(responseObject,nil, self)
            
            
        }
        task.resume()
    }
    
    public func completion(_ completion: NetworkManagerCompletion?) -> Self {
        completionHandler = completion
        
        return self
    }
    
    // MARK: - Class Methods
    
    class func removeNullValues(_ JSONObject: AnyObject) -> AnyObject {
        if JSONObject.isKind(of: NSArray.self) {
            var array: [AnyObject] = []
            
            for object in JSONObject as! NSArray {
                if (object as AnyObject).isKind(of: NSNull.self) == false {
                    array.append(self.removeNullValues(object as AnyObject))
                }
            }
        } else if JSONObject.isKind(of: NSDictionary.self) {
            let dictionary = NSMutableDictionary(dictionary: JSONObject as! NSDictionary)
            
            for (key, value) in JSONObject as! NSDictionary {
                if (value as AnyObject).isKind(of: NSNull.self) {
                    dictionary.removeObject(forKey: key)
                } else if (value as AnyObject).isKind(of: NSArray.self) || (value as AnyObject).isKind(of: NSDictionary.self) {
                    dictionary[key as! String] = self.removeNullValues(value as AnyObject)
                }
            }
            
            return dictionary
        }
        
        return JSONObject
    }
    
    // MARK: - Private Methods
    
    
    func URLSession(_ session: Foundation.URLSession, dataTask: URLSessionDataTask, willCacheResponse proposedResponse: CachedURLResponse, completionHandler: (CachedURLResponse?) -> Void) {
        completionHandler(nil)
    }
}

extension NetworkTask: URLSessionDelegate {
    
    /// SSL Pinning/ URLAuthenticationChallenge
    ///
    /// - Parameters:
    ///   - session: urlSession
    ///   - challenge: urlAuthenticationChallenge
    ///   - completionHandler: completion handler to check whether to allow the request.
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void){
        
        if (networkTaskDelegate?.isSSLNeeded!(for: self.request))!{
            let serverTrust = challenge.protectionSpace.serverTrust
            
            let certificate = SecTrustGetCertificateAtIndex(serverTrust!, 0)
            
            
            // Set SSL policies for domain name check
            
            let policies = NSMutableArray();
            
            policies.add(SecPolicyCreateSSL(true, (challenge.protectionSpace.host as CFString?)))
            
            SecTrustSetPolicies(serverTrust!, policies);
            
            
            
            // Evaluate server certificate
            
            var result: SecTrustResultType = SecTrustResultType(rawValue: 0)!
            
            SecTrustEvaluate(serverTrust!, &result)
            
            let isServerTrusted:Bool = (result == SecTrustResultType.unspecified || result == SecTrustResultType.proceed) || false
            
            
            // Get local and remote cert data
            
            let remoteCertificateData:NSData = SecCertificateCopyData(certificate!)
            
            ///Get path for the ssl certificate
            if let pathToCert = Bundle.main.path(forResource: networkTaskDelegate?.getSSLPath!(), ofType: "der"), let localCertificate = NSData(contentsOfFile: pathToCert) {
                if (isServerTrusted && remoteCertificateData.isEqual(to: localCertificate as Data)) {
                    let credential = URLCredential(trust: serverTrust!)
                    completionHandler(.useCredential, credential)
                }else {
                    logOut()
                    completionHandler(.cancelAuthenticationChallenge, nil)
                }
            }else {
                
                completionHandler(.performDefaultHandling, nil)
            }
        }else {
            completionHandler(.performDefaultHandling, nil)
        }
    }
    
    
    func logOut(){
        networkTaskDelegate?.performActionAfterLogout!()
    }
    
}

extension NetworkTask: URLSessionTaskDelegate {
    
    public func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping(URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            let credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            completionHandler(Foundation.URLSession.AuthChallengeDisposition.useCredential, credential)
        }
    }
}

