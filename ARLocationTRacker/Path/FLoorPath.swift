//
//  FLoorPath.swift
//  ARLocationTRacker
//
//  Created by Sharanabasappa-Macmini on 29/09/17.
//  Copyright © 2017 ymedialabs. All rights reserved.
//

import Foundation
import  SceneKit

extension CGFloat {
    func toRadians() -> CGFloat {
        return self * CGFloat(M_PI) / 180.0
    }
}
class   CylinderLine: SCNNode
{
    
    init( parent: SCNNode,destNode
        : SCNNode,//Needed to line to your scene
        v1: SCNVector3,//Source
        v2: SCNVector3,//Destination
        radius: CGFloat,// Radius of the cylinder
        radSegmentCount: Int, // Number of faces of the cylinder
        color: UIColor,startDirection: String,endDirection : String)// Color of the cylinder
    {
        super.init()
        self.name = "LineNode"
        //Calcul the height of our line
        let  height = v1.distance(from: v2)
        
        //set position to v1 coordonate
        position = v1
        
        //Create the second node to draw direction vector
        let nodeV2 = SCNNode()
        
        //define his position
        nodeV2.position = v2
        
        //add it to parent
        nodeV2.parent?.addChildNode(nodeV2)
        
        let zAlign = SCNNode()
        //zAlign.position.y = 0\
        
        zAlign.eulerAngles.x = Float(CGFloat(M_PI_2))
        
        let cyl1 = SCNBox(width: 3, height: CGFloat(height), length: CGFloat(0.01), chamferRadius: 0.0)
        cyl1.firstMaterial?.diffuse.contents = color
        
        //Create node with cylinder
        let nodeCyl = SCNNode(geometry: cyl1 )
        nodeCyl.name = "Lane"
        // nodeCyl.pivot =  SCNMatrix4MakeTranslation(0, 0, 0)
        nodeCyl.position.y = -height/2
        //  nodeCyl.transform = transform
        
        zAlign.addChildNode(nodeCyl)
        let startdirectionBox = SCNPlane(width: 2, height: 5)//SCNBox(width: 2, height: 5, length: 0.02, chamferRadius: 0.0)
        //Direction materials
        let endDirectionBox = SCNPlane(width: 2, height: 5)
        endDirectionBox.materials = getDirectionMaterial(direction: endDirection)
        startdirectionBox.materials = getDirectionMaterial(direction: "Head")
        
        
        let endNode = SCNNode(geometry: endDirectionBox)
        endNode.name = "NextDirection"
        endNode.eulerAngles.x = -Float(CGFloat(M_PI))
        endNode.position = SCNVector3(0, 1 - height/2  , -1)
        let directionNode = SCNNode(geometry: startdirectionBox)
        directionNode.name = "StartDirection"
        directionNode.eulerAngles.x = -Float(CGFloat(M_PI))
        directionNode.position = SCNVector3(0,height/2 - 10,-1)
        
        //directionNode.transform = SCNMatrix4MakeRotation(0.0,Float.pi / 2 , 1.0, 0)
        nodeCyl.addChildNode(directionNode)
        nodeCyl.addChildNode(endNode)
        
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.fromValue = SCNVector3Make(0,height/2 - 10,-1)
        animation.toValue = SCNVector3Make(0, 2 - height/2,-1)
        animation.duration = 2.0
        // animation.autoreverses = true
        animation.repeatCount = .infinity
        directionNode.addAnimation(animation, forKey: "extrude")
        //directionNode.runAction(moveLoop)
        //Add it to child
        addChildNode(zAlign)
        
        //set constraint direction to our vector
        constraints = [SCNLookAtConstraint(target: nodeV2)]
    }
    func updateNode(destNode : SCNNode,startVector : SCNVector3 , endVector : SCNVector3,startDirection : String,endDirection : String)
    {

        //Calcul the height of our line
        let  height = startVector.distance(from: endVector)
        
        //set position to v1 coordonate
        position = startVector
        
        //Create the second node to draw direction vector
        destNode.position = endVector
        //add it to parent
        guard let boxNode = self.childNode(withName: "Lane", recursively: true) else {
            return
        }
        guard  let boxGeometry = boxNode.geometry as? SCNBox else {
            return
        }
        
        boxGeometry.height = CGFloat(height)
        
         //boxNode.position.y = -height/2
        guard  let startDirectionNode = boxNode.childNode(withName: "StartDirection", recursively: true) else {
            return
        }
        guard  let startPlane = startDirectionNode.geometry as? SCNPlane else {
            return
        }
        startPlane.materials =  self.getDirectionMaterial(direction: startDirection)
        startDirectionNode.geometry = startPlane
        startDirectionNode.removeAllAnimations()
        startDirectionNode.position = SCNVector3(0, 1 - height/2  , -1)
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.fromValue = SCNVector3Make(0,height/2 - 10,-1)
        animation.toValue = SCNVector3Make(0, 2,-1)
        animation.duration = 2.0
        // animation.autoreverses = true
        animation.repeatCount = .infinity
        startDirectionNode.addAnimation(animation, forKey: "extrude")
        guard  let endDirectionNode = boxNode.childNode(withName: "NextDirection", recursively: true) else {
            return
        }
        guard  let endPlane = endDirectionNode.geometry as? SCNPlane else {
            return
        }
        endPlane.materials = self.getDirectionMaterial(direction: endDirection)
        endDirectionNode.geometry = endPlane
        //endDirectionNode.position = SCNVector3(0, height - 2  , -1)
       
       // constraints = [SCNLookAtConstraint(target: destNode)]
    }
    func getDirectionMaterial (direction:String) -> [SCNMaterial]{
        let sphereMaterial = SCNMaterial()
        
        switch direction {
        case "turn-left" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_invalid_left")
        case  "uturn-left" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_uturn")
        case  "uturn-right" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_roundabout_sharp_right")
        case  "turn-slight-right" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_turn_slight_right")
        case  "turn-slight-left" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_turn_slight_left")
        case  "turn-sharp-right" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_notificaiton_sharp_right")
        case  "turn-sharp-left" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_new_name_sharp_left")
        case  "turn-right" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_on_ramp_right")
        case  "straight" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "up")
        case  "roundabout-right" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_roundabout_right")
        case  "roundabout-left" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_roundabout_left")
        case  "ramp-right" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_off_ramp_right")
        case  "ramp-left" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_off_ramp_left")
        case  "keep-right" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_continue_right")
        case  "keep-left" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_invalid_left")
        case  "fork-right" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_off_ramp_right")
        case  "fork-left" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_fork_left")
        case "South":
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "up")
        case "North":
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "up")
        case "Head":
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "up")
        default:
            sphereMaterial.diffuse.contents = UIColor.red
            break
        }
        sphereMaterial.isDoubleSided = true
        return [sphereMaterial]
    }
    override init() {
        super.init()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func distanceBetweenPoints2(A: SCNVector3, B: SCNVector3) -> CGFloat {
        let l = sqrt(
            (A.x - B.x) * (A.x - B.x)
                +   (A.y - B.y) * (A.y - B.y)
                +   (A.z - B.z) * (A.z - B.z)
        )
        return CGFloat(l)
    }
}

class DrawCurve :SCNNode {
    
    init( parent: SCNNode,//Needed to line to your scene
        v1: SCNVector3,//Source
        v2: SCNVector3,//Destination
        radius: CGFloat,// Radius of the cylinder
        radSegmentCount: Int, // Number of faces of the cylinder
        color: UIColor,angle:Float ,startDirection: String,endDirection : String){
        super.init()
        //Calcul the height of our line
        let  height = v1.distance(from: v2)
        
        //set position to v1 coordonate
        position = v1
        
        //Create the second node to draw direction vector
        let nodeV2 = SCNNode()
        
        //define his position
        nodeV2.position = v2
        
        //add it to parent
        nodeV2.parent?.addChildNode(nodeV2)
        
        var path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        //        path.addArc(withCenter: CGPoint(x: Int(v2.x - v2.x / 2), y: Int(v2.z - v1.z / 2)), radius: CGFloat(height / 2.0), startAngle: <#T##CGFloat#>, endAngle: <#T##CGFloat#>, clockwise: <#T##Bool#>)
    }
    override init() {
        super.init()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
class Path1: SCNNode
{
    init( parent: SCNNode,//Needed to line to your scene
        v1: SCNVector3,//Source
        v2: SCNVector3,//Destination
        radius: CGFloat,// Radius of the cylinder
        radSegmentCount: Int, // Number of faces of the cylinder
        color: UIColor ,angle : Float )// Color of the cylinder
    {
        super.init()
        
        //Calcul the height of our line
        let  height = v1.distance(from: v2)
        
        //set position to v1 coordonate
        position = v1
        //        worldPosition = v1
        
        //Create the second node to draw direction vector
        let nodeV2 = SCNNode()
        
        //define his position
        nodeV2.position = v2
        //add it to parent
        parent.addChildNode(nodeV2)
        let translation = SCNMatrix4Identity
        // Rotate (yaw) around y axis
        let rotation = SCNMatrix4MakeRotation(-angle, 0, 1, 0)
        // Final transformation: TxR
        let transform = SCNMatrix4Mult(translation, rotation)
        
        //Align Z axis
        let zAlign = SCNNode()
        
        //zAlign.eulerAngles.x = Float(M_PI_2)
        // zAlign.transform = rotation
        // zAlign.position.y = -4
        //create our cylinder
        let cyl = SCNBox(width: 1, height: CGFloat(height), length: CGFloat(0.01), chamferRadius: 0.0)
        
        //SCNCylinder(radius: radius, height: CGFloat(height))
        
        // Rotate (yaw) around y axis
        
        
        // let transform = SCNMatrix4Mult(translation, rotation)
        
        // cyl.radialSegmentCount = radSegmentCount
        cyl.firstMaterial?.diffuse.contents = color
        
        //Create node with cylinder
        let nodeCyl = SCNNode(geometry: cyl )
        
        
        
        nodeCyl.transform = transform
        nodeCyl.eulerAngles.x = -Float(M_PI_2)
        //zAlign.eulerAngles.y = Float(M_PI_2)
        zAlign.addChildNode(nodeCyl)
        
        //Add it to child
        addChildNode(zAlign)
        
        //set constraint direction to our vector
        constraints = [SCNLookAtConstraint(target: nodeV2)]
    }
    
    override init() {
        super.init()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
//extension code starts

func normalizeVector(_ iv: SCNVector3) -> SCNVector3 {
    let length = sqrt(iv.x * iv.x + iv.y * iv.y + iv.z * iv.z)
    if length == 0 {
        return SCNVector3(0.0, 0.0, 0.0)
    }
    
    return SCNVector3( iv.x / length, iv.y / length, iv.z / length)
    
}

extension SCNNode {
    
    func buildLineInTwoPointsWithRotation(from startPoint: SCNVector3,
                                          to endPoint: SCNVector3,
                                          radius: CGFloat,
                                          color: UIColor) -> SCNNode {
        let w = SCNVector3(x: endPoint.x-startPoint.x,
                           y: endPoint.y-startPoint.y,
                           z: endPoint.z-startPoint.z)
        let l = CGFloat(sqrt(w.x * w.x + w.y * w.y + w.z * w.z))
        
        if l == 0.0 {
            // two points together.
            let sphere = SCNSphere(radius: radius)
            sphere.firstMaterial?.diffuse.contents = color
            self.geometry = sphere
            self.position = startPoint
            return self
            
        }
        
        let cyl = SCNBox(width: 1, height: l, length: 0.01, chamferRadius: 0.0)
        //SCNCylinder(radius: radius, height: l)
        cyl.firstMaterial?.diffuse.contents = color
        
        self.geometry = cyl
        
        //original vector of cylinder above 0,0,0
        let ov = SCNVector3(0, 1/2.0,0)
        //target vector, in new coordination
        let nv = SCNVector3((endPoint.x - startPoint.x)/2.0, (endPoint.y - startPoint.y)/2.0,
                            (endPoint.z-startPoint.z)/2.0)
        
        // axis between two vector
        let av = SCNVector3( (ov.x + nv.x)/2.0, (ov.y+nv.y)/2.0, (ov.z+nv.z)/2.0)
        
        //normalized axis vector
        let av_normalized = normalizeVector(av)
        let q0 = Float(0.0) //cos(angel/2), angle is always 180 or M_PI
        let q1 = Float(av_normalized.x) // x' * sin(angle/2)
        let q2 = Float(av_normalized.y) // y' * sin(angle/2)
        let q3 = Float(av_normalized.z) // z' * sin(angle/2)
        
        let r_m11 = q0 * q0 + q1 * q1 - q2 * q2 - q3 * q3
        let r_m12 = 2 * q1 * q2 + 2 * q0 * q3
        let r_m13 = 2 * q1 * q3 - 2 * q0 * q2
        let r_m21 = 2 * q1 * q2 - 2 * q0 * q3
        let r_m22 = q0 * q0 - q1 * q1 + q2 * q2 - q3 * q3
        let r_m23 = 2 * q2 * q3 + 2 * q0 * q1
        let r_m31 = 2 * q1 * q3 + 2 * q0 * q2
        let r_m32 = 2 * q2 * q3 - 2 * q0 * q1
        let r_m33 = q0 * q0 - q1 * q1 - q2 * q2 + q3 * q3
        
        self.transform.m11 = r_m11
        self.transform.m12 = r_m12
        self.transform.m13 = r_m13
        self.transform.m14 = 0.0
        
        self.transform.m21 = r_m21
        self.transform.m22 = r_m22
        self.transform.m23 = r_m23
        self.transform.m24 = 0.0
        
        self.transform.m31 = r_m31
        self.transform.m32 = r_m32
        self.transform.m33 = r_m33
        self.transform.m34 = 0.0
        
        self.transform.m41 = (startPoint.x + endPoint.x) / 2.0
        self.transform.m42 = (startPoint.y + endPoint.y) / 2.0
        self.transform.m43 = (startPoint.z + endPoint.z) / 2.0
        self.transform.m44 = 1.0
        return self
    }
}
