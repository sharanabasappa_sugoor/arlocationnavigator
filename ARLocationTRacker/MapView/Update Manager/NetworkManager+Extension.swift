//
//  NetworkManager+Extension.swift
//  Smartify
//
//  Created by Sharanabasappa-Macmini on 14/09/17.
//  Copyright © 2017 ymedialabs. All rights reserved.
//

import Foundation
import RSClient
let baseURL = "http://smartifiy-dev.ymedia.in"
enum APIEndpoint {
    case FaceDetection
    case EmployessList
    case CaptureUserDetails
    func getApiEndPoint()->String{
        switch self {
        case .FaceDetection:
            
            return baseURL + "/write/api/v1/compare"
        case .EmployessList:
            return baseURL + "/read/api/v1/getAllEmployees"
        case.CaptureUserDetails :
            return baseURL + "/write/api/v1/captureDetails"
        
        }
    }
}

extension NetworkTask : NetworkTasksDelegate {
    public func isSSLNeeded(for request: URLRequest?) -> Bool {
        // Handle action
        return true
    }
    
    public func performActionAfterLogout() {
        // Handle action
    }
    
    public func getSSLPath() -> String{
        return "chefd_ssl"
    }
    
}

extension UIViewController {
    @discardableResult
func customActivityIndicatory(_ viewContainer: UIView, startAnimate:Bool? = true) -> UIActivityIndicatorView {
    let mainContainer: UIView = UIView(frame: viewContainer.frame)
    mainContainer.center = viewContainer.center
    mainContainer.backgroundColor = UIColor.white
    mainContainer.alpha = 0.5
    mainContainer.tag = 789456123
    mainContainer.isUserInteractionEnabled = false
    
    let viewBackgroundLoading: UIView = UIView(frame: CGRect(x:0,y: 0,width: 80,height: 80))
    viewBackgroundLoading.center = viewContainer.center
    viewBackgroundLoading.backgroundColor = UIColor(red: 68 / 255, green: 68 / 255, blue: 68 / 255, alpha: 0.5)
    viewBackgroundLoading.alpha = 0.5
    viewBackgroundLoading.clipsToBounds = true
    viewBackgroundLoading.layer.cornerRadius = 15
    
    let activityIndicatorView: UIActivityIndicatorView = UIActivityIndicatorView()
    activityIndicatorView.frame = CGRect(x:0.0,y: 0.0,width: 40.0, height: 40.0)
    activityIndicatorView.activityIndicatorViewStyle =
        UIActivityIndicatorViewStyle.whiteLarge
    activityIndicatorView.center = CGPoint(x: viewBackgroundLoading.frame.size.width / 2, y: viewBackgroundLoading.frame.size.height / 2)
    if startAnimate!{
        viewBackgroundLoading.addSubview(activityIndicatorView)
        mainContainer.addSubview(viewBackgroundLoading)
        viewContainer.addSubview(mainContainer)
        activityIndicatorView.startAnimating()
    }else{
        for subview in viewContainer.subviews{
            if subview.tag == 789456123{
                subview.removeFromSuperview()
            }
        }
    }
    return activityIndicatorView
}
}
