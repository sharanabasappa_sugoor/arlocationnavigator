//
//  UpdateManager.swift
//  ARLocationTRacker
//
//  Created by Sharanabasappa-Macmini on 26/09/17.
//  Copyright © 2017 ymedialabs. All rights reserved.
//

import Foundation
import UIKit
// Add your app url to redirect t a
var KAppStoreURl : String = "https://itunes.apple.com/us/app/liveamoment/id1237058310?mt=8&uo=4"
extension UIAlertController {
    
    func show() {
        present(true, completion: nil)
    }
    
    func present(_ animated: Bool, completion: (() -> Void)?) {
        if let rootVC = UIApplication.shared.keyWindow?.rootViewController {
            presentFromController(rootVC, animated: animated, completion: completion)
        }
    }
    
    fileprivate func presentFromController(_ controller: UIViewController, animated: Bool, completion: (() -> Void)?) {
        if  let navVC = controller as? UINavigationController,
            let visibleVC = navVC.visibleViewController {
            presentFromController(visibleVC, animated: animated, completion: completion)
        } else {
            if  let tabVC = controller as? UITabBarController,
                let selectedVC = tabVC.selectedViewController {
                presentFromController(selectedVC, animated: animated, completion: completion)
            } else {
                controller.present(self, animated: animated, completion: completion)
            }
        }
    }
}
extension UIViewController {
    
    func updateAlert(_ alertString : String , isSoftUpdate : Bool,completion: @escaping () -> Void){
        let alert = UIAlertController(title: "Update Alert", message:alertString,  preferredStyle: UIAlertControllerStyle.alert)
        let updateAction = UIAlertAction(title: "Update", style: .default) { (action) in
            completion()
        }
        
        
        alert.addAction(updateAction)
        if isSoftUpdate {
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler : nil))
        }
        DispatchQueue.main.async {
            alert.present(true, completion: nil)
        }
        
        
    }
}
extension String {
    func versionToInt() -> [Int] {
        return self.components(separatedBy: ".")
            .map {
                Int.init($0) ?? 0
        }
    }
}

enum UpdateVersion :Int {
    case ThirdLaunchsVersion = 1
    case SecondLaunchsVersion = 2
    case FirstLauncsVersion = 3
    case EveryTimeLaunchVersion = 4
    
    func popUpLauch() -> Int{
        switch  self {
        case .EveryTimeLaunchVersion:
            return 1
        case .FirstLauncsVersion:
            return 1
        case .SecondLaunchsVersion :
            return 2
        case.ThirdLaunchsVersion:
            return 3
        }
    }
}
enum VersionError: Error {
    case invalidResponse, invalidBundleInfo
}
class APPUpdateManager : UIViewController{
    
    static var  sharedInstatnce = APPUpdateManager()
    func checkForUpdates(){
        
        _ = try? isUpdateAvailable { (version, newVersion,error) in
            if let error = error {
                print(error)
            } else if let version = version {
                
                let currentVersion = UserDefaults.standard.value(forKey: "LastUpdateVersion") as? String
                if  currentVersion != nil && currentVersion ==  newVersion   {
                    if let current = UserDefaults.standard.value(forKey: "NoLaunches") as? Int{
                        UserDefaults.standard.set(current + 1, forKey: "NoLaunches")
                    }
                    else {
                        UserDefaults.standard.set(1, forKey: "NoLaunches")
                        
                    }
                    UserDefaults.standard.synchronize()
                    if version >= UpdateVersion.EveryTimeLaunchVersion.rawValue {
                        
                        self.updateAlert("alert", UpdateVersion.EveryTimeLaunchVersion.popUpLauch(), false)
                        // DO the Hard Update Avaliable without cancel update
                    }
                    else if version == UpdateVersion.SecondLaunchsVersion.rawValue{
                        self.updateAlert("alert", UpdateVersion.SecondLaunchsVersion.popUpLauch(), true)
                        
                        // show the pop up every first time  opens with cancel update
                    }
                    else if version == UpdateVersion.ThirdLaunchsVersion.rawValue{
                        self.updateAlert("alert", UpdateVersion.ThirdLaunchsVersion.popUpLauch(), true)
                        
                        // show the pop up every third time  opens with cancel update
                    }
                    else if version == UpdateVersion.FirstLauncsVersion.rawValue{
                        self.updateAlert("alert", UpdateVersion.FirstLauncsVersion.popUpLauch(), true)
                        // show the pop up every first time  opens with cancel update
                    }
                }
                else {
                    
                    UserDefaults.standard.set(newVersion, forKey: "LastUpdateVersion")
                    UserDefaults.standard.set(1, forKey: "NoLaunches")
                    UserDefaults.standard.synchronize()
                    self.updateAlert("alert", isSoftUpdate: version < UpdateVersion.EveryTimeLaunchVersion.rawValue , completion: {
                        
                        let appstoreURL = URL.init(string: KAppStoreURl)
                        guard let appurl = appstoreURL else {
                            return
                        }
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(appurl, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(appurl)
                        }
                    })
                }
                
            }
        }
    }
    
    func updateAlert(_ alertSubString : String, _ launchTimes : Int ,_ isSoftUpdate : Bool ){
        if let numberLaunches = UserDefaults.standard.value(forKey: "NoLaunches") as? Int {
            if numberLaunches % launchTimes == 0  {
                self.updateAlert("alert", isSoftUpdate: isSoftUpdate, completion: {
                    
                    let appstoreURL = URL.init(string: KAppStoreURl)
                    guard let appurl = appstoreURL else {
                        return
                    }
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(appurl, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(appurl)
                    }
                    
                })
            }
        }
        
    }
    func isUpdateAvailable(completion: @escaping (Int?,String,Error?) -> Void) throws -> URLSessionDataTask {
        
        guard let info = Bundle.main.infoDictionary,
            let currentVersion = info["CFBundleShortVersionString"] as? String,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
                //
                throw VersionError.invalidBundleInfo
        }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            do {
                if let error = error { throw error }
                guard let data = data else { throw VersionError.invalidResponse }
                let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any]
                guard let result = (json?["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String else {
                    throw VersionError.invalidResponse
                }
                KAppStoreURl = (result["trackViewUrl"] as? String)!
                //=
                //                if requiredVersion.compare(actualVersion, options: .numeric, range: nil, locale: .current) == .orderedDescending {
                //                    // actualVersion is lower than the requiredVersion
                //                }
                var currentVerArr = currentVersion.versionToInt()
                var storeVerArr =  version.versionToInt()
                var  countDiff = storeVerArr.count - currentVerArr.count
                //these is used to make both array of equal size with rest fields filled with zero's
                if countDiff > 0 {
                    while countDiff != 0  {
                        currentVerArr.append(0)
                        countDiff = countDiff - 1
                    }
                }
                else if countDiff < 0 {
                    while countDiff != 0  {
                        storeVerArr.append(0)
                        countDiff = countDiff + 1
                    }
                }
   
                if let currentSum = Int(currentVerArr.map(String.init).joined()), let storeSum = Int(storeVerArr.map(String.init).joined()) {
                   completion(storeSum - currentSum , currentVersion,nil)
                }
         
            } catch {
                completion(nil,"" ,error)
            }
        }
        task.resume()
        return task
    }
    
}
