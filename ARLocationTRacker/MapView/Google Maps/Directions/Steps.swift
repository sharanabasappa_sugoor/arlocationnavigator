//
//  StepModel.swift
//  ARLocationTRacker
//
//  Created by Sharanabasappa-Macmini on 28/09/17.
//  Copyright © 2017 ymedialabs. All rights reserved.
//

import UIKit
import CoreLocation
import  RSClient
class Steps {
    var direction = "South"
    var nextDirection :String?
    var startlocation : CLLocation = CLLocation()
    var endLocation : CLLocation = CLLocation()
    var points:String = String()
    init(_ steps : JSON) {
        if let startLoc = steps["start_location"].dictionary{
            startlocation = CLLocation(latitude: (startLoc["lat"]?.doubleValue)!, longitude: (startLoc["lng"]?.doubleValue)!)
        }
        if let endloc = steps["end_location"].dictionary{
            endLocation = CLLocation(latitude: (endloc["lat"]?.doubleValue)!, longitude: (endloc["lng"]?.doubleValue)!)
        }
        if let dir = steps["maneuver"].string {
            self.direction = dir
        }
        if let polyline =   steps["polyline"].dictionary {
            if let point = polyline["points"]?.string {
                self.points = point
            }
        }
    }
}
