//
//  GoogleDirections.swift
//  ARLocationTRacker
//
//  Created by Sharanabasappa-Macmini on 28/09/17.
//  Copyright © 2017 ymedialabs. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import GooglePlaces
import RSClient
let KDirectionsKey = "AIzaSyD1mnn0tOe6pnooCUz1sUwyLKSLcE9zRIQ"
//https://maps.googleapis.com/maps/api/directions/outputFormat?parameters
//https://maps.googleapis.com/maps/api/directions/json?origin=Toronto&destination=Montreal&key=YOUR_API_KEY
class GoogelDirections {
    
    class func getparameters(_ startLocation : CLLocationCoordinate2D, _ endLocation : CLLocationCoordinate2D) -> String {
        let baseUrl = "https://maps.googleapis.com/maps/api/directions/json?"
        let origin = "origin=\(startLocation.latitude),\(startLocation.longitude)"
        let destination = "destination=\(endLocation.latitude),\(endLocation.longitude)"
        
        return  baseUrl + origin + "&" + destination + "&" + "key=\(KDirectionsKey)"+"&mode=walking"
        
    }
    class func getDirections(_ startLocation : CLLocationCoordinate2D, _ endLocation : CLLocationCoordinate2D,completion: @escaping ([Steps],Error?) -> Void){
        var stepsArray = [Steps]()
        let request = NetworkManager.requestForURL(GoogelDirections.getparameters(startLocation, endLocation), method: .Get, params: nil, headers: nil, encoding: .Raw)
        _ = NetworkManager.request(request as URLRequest).completion({ (response, error, task) -> () in
            
            if  error == nil {
                
                for route in response!["routes"].arrayValue {
                    for leg in route["legs"].arrayValue {
                        // debugPrint(leg)
                        for step in leg["steps"].arrayValue {
                            let stepObj = Steps(step)
                            debugPrint("stepDir:- \(stepObj.direction)")
                            stepsArray.append(stepObj)
                            
                        }
                        completion(stepsArray, error)
                    }
                    
                }
            }
            else{
                completion(stepsArray, error)
            }
        })
        
        
    }
}
