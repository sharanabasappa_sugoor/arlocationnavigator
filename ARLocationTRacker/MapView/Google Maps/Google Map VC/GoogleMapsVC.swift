//
//  GoogleMapsVC.swift
//  ARLocationTRacker
//
//  Created by Sharanabasappa-Macmini on 21/09/17.
//  Copyright © 2017 ymedialabs. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

//completionHanlder VC
 typealias stepsHandler = (_ steps : [Steps]) -> ()
class GoogleMapsVC: UIViewController{
    
    @IBOutlet weak var preViewMapView: UIView!
    var stepsHadlerCompletion : stepsHandler?
    var currentLocation: CLLocation?
    var annotainsList : [CLLocation] = [CLLocation]()
    var mapView: GMSMapView!
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 15.0
    // An array to hold the list of likely places.
    var likelyPlaces: [GMSPlace] = []
    var  infoMarker :GMSMarker = GMSMarker()
    // The currently selected place.
    var selectedPlace: GMSPlace?
    var markers = [GMSMarker]()
    var tappedMarker = GMSMarker()
    var infoWindow : CustomAnnotView?
    override func viewDidLoad() {
        super.viewDidLoad()
        infoWindow = Bundle.main.loadNibNamed("CustomAnnotView", owner: nil, options: nil)?.first as? CustomAnnotView
    
        placesClient = GMSPlacesClient.shared()
        guard  let cur = LocationManager.sharedInstance.currentLocation else {
            return
        }
        DispatchQueue.main.async {
            let camera = GMSCameraPosition.camera(withLatitude: cur.coordinate.latitude, longitude:cur.coordinate.longitude, zoom: 6.0)
            self.mapView = GMSMapView.map(withFrame: self.preViewMapView.bounds, camera: camera)
            self.mapView.delegate = self
            self.mapView.isIndoorEnabled = true
            self.mapView.isMyLocationEnabled = true
            self.mapView.mapType = .normal
            self.mapView.settings.myLocationButton = true
            self.mapView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
            self.preViewMapView.addSubview(self.mapView)
            self.infoMarker.position =  cur.coordinate
            self.infoMarker.icon = GMSMarker.markerImage(with: .black)
            //        infoMarker.iconView = CustomAnnotView(frame: CGRect(x: 0.0, y: 0.0, width: 150.0, height: 200.0))
            //        //   marker.appearAnimation = kGMSMarkerAnimationPop
            //        infoMarker.title = "Sydney"
            //        infoMarker.snippet = "Australia"
            self.infoMarker.map = self.mapView
        }
  
        
     
        let defaults = UserDefaults.standard
        
        let placesData = defaults.object(forKey: "Places") as? NSData
        
        if let placesData = placesData {
            let placesArray = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as? [CLLocation]
            
            if let placesArray = placesArray {
                
                annotainsList = placesArray
                for annotaion in annotainsList {
                    self.addAnotation(annotaion.coordinate)
                }
                
            }
            
        }
        
        
        //        markers.append(infoMarker)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func addAnotation(_ location : CLLocationCoordinate2D){
        DispatchQueue.main.async {
            let london = GMSMarker(position: location)
            
            london.map = self.mapView
            london.icon = GMSMarker.markerImage(with: .black)
            // london.infoWindowAnchor = CGPoint(x: 0., y: 0.2)
            
            london.userData = location
            london.map = self.mapView
            
            //        mapView.selectedMarker = london
            self.markers.append(london)

        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    func likeLyHoodPlaces(){
        placesClient.currentPlace { (placeList, error) in
            if let error = error {
                print("error")
            }
            // Get likely places and add to the list.
            if let likelihoodList = placeList {
                for likelihood in likelihoodList.likelihoods {
                    let place = likelihood.place
                    self.likelyPlaces.append(place)
                }
            }
        }
    }

    
    
    @IBAction func DoneAction(_ sender: Any) {
        DispatchQueue.main.async {
              self.navigationController?.popViewController(animated: true)
        }
      
    }
    
}
extension GoogleMapsVC : GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTapPOIWithPlaceID placeID: String,
                 name: String, location: CLLocationCoordinate2D) {
        print("You tapped \(name): \(placeID), \(location.latitude)/\(location.longitude)")
        
        let london = GMSMarker(position: location)
        
        london.map = mapView
        london.icon = GMSMarker.markerImage(with: .black)
        london.infoWindowAnchor = CGPoint(x: 0.5, y: 0.2)
        
        london.userData = location
        london.map = mapView
        mapView.selectedMarker = london
        annotainsList.append(CLLocation(latitude: location.latitude, longitude: location.longitude))
        let defaults = UserDefaults.standard
        if annotainsList.count > 0 {
            let placesData1 = NSKeyedArchiver.archivedData(withRootObject: annotainsList)
            defaults.set(placesData1, forKey: "Places")
            UserDefaults.standard.synchronize()
        }
        //        if !annotainsList.contains(CLLocation(latitude: (mapVIew?.centerCoordinate.latitude)!, longitude: (mapVIew?.centerCoordinate.longitude)!)) {
        //            annotainsList.append(CLLocation(latitude: (mapVIew?.centerCoordinate.latitude)!, longitude: (mapVIew?.centerCoordinate.longitude)!))
        //        }
        //
        
        // addAnotation((mapVIew?.centerCoordinate)!)
        // mapVIew?.add(MKCircle(center: (mapVIew?.centerCoordinate)!, radius: 50))
        markers.append(london)
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        return UIView()
    }
    
    // reset custom infowindow whenever marker is tapped
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let location = marker.userData as? CLLocationCoordinate2D
        let defaults = UserDefaults.standard
        tappedMarker = marker

        DispatchQueue.main.async {
            self.infoWindow?.removeFromSuperview()
        }
        infoWindow =  Bundle.main.loadNibNamed("CustomAnnotView", owner: nil, options: nil)?.first as? CustomAnnotView
        infoWindow?.locationCordinate = marker.position
        infoWindow?.delete = { (location) in
            let placesData = defaults.object(forKey: "Places") as? NSData
            
            if let placesData = placesData {
                let placesArray = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as? [CLLocation]
                
                if placesArray != nil {
                    //                         self.annotainsList = placesArray
                    let filterdAnnotaions = self.annotainsList.map({ $0.coordinate })
                    let location = CLLocation(latitude: (marker.position.latitude), longitude: (marker.position.longitude))
                    var index = 0
                    for annotati in filterdAnnotaions {
                        if annotati.isEqual(location.coordinate) {
                            self.annotainsList.remove(at: index)
                        }
                        index = index + 1
                    }
                    
                }
                
                if self.annotainsList.count > 0 {
                    let placesData = NSKeyedArchiver.archivedData(withRootObject: self.annotainsList)
                    defaults.set(placesData, forKey: "Places")
                    
                }
                else{
                    defaults.set(nil, forKey: "Places")
                }
                UserDefaults.standard.synchronize()
                
            }
            for annot in self.markers {
                if annot.position.isEqual((self.infoWindow?.locationCordinate)!) {
                    let index = self.markers.index(of: annot)
                    if let index = index {
                        self.markers[index].map = nil
                        self.infoWindow?.removeFromSuperview()
                    }
                    //self.mapVIew?.remove()
                }
            }
            
        }
        infoWindow?.navigate = { (location) in
            let loc =  CLLocation(latitude: (marker.position.latitude), longitude: (marker.position.longitude))
            let placesData = NSKeyedArchiver.archivedData(withRootObject:loc )
            defaults.set(placesData, forKey: "TrackLocation")
            defaults.synchronize()
            guard  let cur = LocationManager.sharedInstance.currentLocation else {
                return
            }
            DispatchQueue.main.async {
                self.customActivityIndicatory(self.view , startAnimate: true)
            }
            GoogelDirections.getDirections(cur.coordinate, marker.position, completion: { (steps, error) in
                var directions = [String]()
                var controlPointsLocs = [CLLocation]()
                directions.removeAll()
                controlPointsLocs.removeAll()
                var endDir :String? = nil
                var index = 0
                while index < steps.count {
                    if index + 1  < steps.count {
                        steps[index].nextDirection = steps[index + 1].direction
                    }
                    index = index + 1 
                }
                for step in steps {
                    debugPrint("direction:- \(step.nextDirection)")
                    if controlPointsLocs.count == 0 {
                           directions.append(step.direction)
                           directions.append(step.direction)
                        controlPointsLocs.append(step.startlocation)
                        controlPointsLocs.append(step.endLocation)
                    }
                    else {
                        if (controlPointsLocs.last?.coordinate.isEqual(step.startlocation.coordinate))!{
                               directions.append(step.direction)
                            controlPointsLocs.append(step.endLocation)
                        }
                        else{
                               directions.append(step.direction)
                               directions.append(step.direction)
                            controlPointsLocs.append(step.startlocation)
                            controlPointsLocs.append(step.endLocation)
                        }
                    }
                    
//                                        if let lastStep = steps.last {
//                                            if step.endLocation.coordinate.isEqual(lastStep.endLocation.coordinate) {
//                                                  directions.append(step.direction)
//                                              controlPointsLocs.append(step.endLocation)
//                                            }
//                                        }
                    
                }
                if error == nil {
                    let placesData = NSKeyedArchiver.archivedData(withRootObject:controlPointsLocs)
                    let directionData = NSKeyedArchiver.archivedData(withRootObject:directions)
                    defaults.set(placesData, forKey: "Steps")
                    defaults.set(directionData, forKey: "Directions")
                    defaults.synchronize()
                    
                }
                
                DispatchQueue.main.async {
                    self.customActivityIndicatory(self.view , startAnimate: false)
                
                    self.stepsHadlerCompletion!(steps)
                    self.navigationController?.popViewController(animated: true)
                }
                
            })
            
        }
         DispatchQueue.main.async {
            self.view.addSubview(self.infoWindow!)
        }
        return false
    }
    
    // let the custom infowindow follows the camera
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (tappedMarker.userData != nil){
            let location = tappedMarker.userData as! CLLocationCoordinate2D
            infoWindow?.center = mapView.projection.point(for: location)
            infoWindow?.center.y -= 60
        }
    }
    
    // take care of the close event
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        infoWindow?.removeFromSuperview()
    }
    //      func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
    //        print(marker.iconView)
    //        print("test")
    //
    //    }
}
