//
//  ADDLocationVC.swift
//  ARKitCompassRose
//
//  Created by Sharanabasappa-Macmini on 12/09/17.
//  Copyright © 2017 vasile.ch. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
extension CLLocationCoordinate2D {
    func isEqual(_ coord: CLLocationCoordinate2D) -> Bool {
        return (fabs(self.latitude - coord.latitude) < .ulpOfOne) && (fabs(self.longitude - coord.longitude) < .ulpOfOne)
    }
}

class ADDLocationVC: UIViewController,LocationManagerDelegate{
    var annotainsList : [CLLocation] = [CLLocation]()
    @IBOutlet weak var mapVIew: MKMapView?
    override func viewDidLoad() {
        super.viewDidLoad()
        mapVIew?.delegate = self
        mapVIew?.showsUserLocation = true
        self.navigationController?.navigationBar.isHidden = true
        //        LocationManager.sharedInstance.delegate = self
        //
        //        LocationManager.sharedInstance.locationManager.requestWhenInUseAuthorization()
        //LocationManager.sharedInstance.startUpdating()
        let defaults = UserDefaults.standard
        
        let placesData = defaults.object(forKey: "Places") as? NSData
        
        if let placesData = placesData {
            let placesArray = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as? [CLLocation]
            
            if let placesArray = placesArray {
                
                annotainsList = placesArray
                for annotaion in annotainsList {
                    self.addAnotation(annotaion.coordinate)
                }
                
            }
            
        }
        
        guard  let current = LocationManager.sharedInstance.currentLocation?.coordinate else {
          return
        }
        addAnotation((current))
        // Do any additional setup after loading the view.
     
    }
    func addAnotation(_ location : CLLocationCoordinate2D){
        
        let region = MKCoordinateRegion(center: location, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        self.mapVIew?.setRegion(region, animated: true)
        
        let pointAnnotation = MKPointAnnotation()
        pointAnnotation.coordinate = location
        pointAnnotation.title = ""
        mapVIew?.addAnnotation(pointAnnotation)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    @IBAction func DoneAction(_ sender: Any) {
        let defaults = UserDefaults.standard
        if !annotainsList.contains(CLLocation(latitude: (mapVIew?.centerCoordinate.latitude)!, longitude: (mapVIew?.centerCoordinate.longitude)!)) {
            annotainsList.append(CLLocation(latitude: (mapVIew?.centerCoordinate.latitude)!, longitude: (mapVIew?.centerCoordinate.longitude)!))
        }
        
        if annotainsList.count > 0 {
            let placesData1 = NSKeyedArchiver.archivedData(withRootObject: annotainsList)
            defaults.set(placesData1, forKey: "Places")
            UserDefaults.standard.synchronize()
        }
        addAnotation((mapVIew?.centerCoordinate)!)
        mapVIew?.add(MKCircle(center: (mapVIew?.centerCoordinate)!, radius: 50))
        
    }
    
    @IBAction func cancel(_ sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func addAnnotation(_ sender: Any) {
        
        annotainsList.append(CLLocation(latitude: (mapVIew?.centerCoordinate.latitude)!, longitude: (mapVIew?.centerCoordinate.longitude)!))
        
        addAnotation((mapVIew?.centerCoordinate)!)
        mapVIew?.add(MKCircle(center: (mapVIew?.centerCoordinate)!, radius: 50))
        
    }
    
    
}
extension ADDLocationVC : MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        else {
            let annotationView = CustomAnnotationView(annotation:annotation, reuseIdentifier:  "myGeossstification") 
            annotationView.canShowCallout = true
            
            // annotationView?.detailCalloutAccessoryView =  myView
            
            annotationView.customView()
             let defaults = UserDefaults.standard
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "home2.png")
           
            annotationView.locationCordinate = annotation.coordinate
            annotationView.myview?.navigate = { (location) in
                let loc = CLLocation(latitude: (annotationView.locationCordinate?.latitude)!, longitude: (annotationView.locationCordinate?.longitude)!)
                let placesData = NSKeyedArchiver.archivedData(withRootObject:loc )
                defaults.set(placesData, forKey: "TrackLocation")
                defaults.synchronize()
                self.navigationController?.popViewController(animated: true)
            }
            annotationView.myview?.delete = { (location) in
               
                let placesData = defaults.object(forKey: "Places") as? NSData
                
                if let placesData = placesData {
                    let placesArray = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as? [CLLocation]
                    
                    if let placesArray = placesArray {
                        //                         self.annotainsList = placesArray
                        let filterdAnnotaions = self.annotainsList.map({ $0.coordinate })
                        let location =  CLLocation(latitude:  (annotationView.locationCordinate?.latitude)!, longitude: (annotationView.locationCordinate?.longitude)!)
                        var index = 0
                        for annotati in filterdAnnotaions {
                            if annotati.isEqual(location.coordinate) {
                                self.annotainsList.remove(at: index)
                            }
                            index = index + 1
                        }
                
                    }
                    
                    if self.annotainsList.count > 0 {
                        let placesData = NSKeyedArchiver.archivedData(withRootObject: self.annotainsList)
                        defaults.set(placesData, forKey: "Places")
                       
                    }
                    else{
                        defaults.set(nil, forKey: "Places")
                    }
                     UserDefaults.standard.synchronize()
                
                }
                for annot in (self.mapVIew?.annotations)! {
                    if annot.coordinate.isEqual(annotationView.locationCordinate!) {
                        self.mapVIew?.removeAnnotation(annot )
                        //self.mapVIew?.remove()
                    }
                }
               
            }
            return annotationView

        }
        
       
    }
    


//func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
  //  let renderer = MKCircleRenderer(overlay: overlay)
  //  renderer.fillColor = UIColor.black.withAlphaComponent(0.3)
  //  renderer.strokeColor = UIColor.blue
  //  renderer.lineWidth = 2
  //  return renderer
//}
func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
    // Delete geotification
    // let geotification = view.annotation as! Geotification
    // remove(geotification: geotification)
    //saveAllGeotifications()
}
func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
    if let annotation = view.annotation {
        print(annotation.coordinate)
    }
}

func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
    // let region = MKCoordinateRegion(center: (self.mapView?.userLocation.coordinate)!, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
    //mapView.setRegion(region, animated: true)
}
}
