//
//  BaseNode.swift
//  ARLocationTRacker
//
//  Created by Sharanabasappa-Macmini on 10/10/17.
//  Copyright © 2017 ymedialabs. All rights reserved.
//
import UIKit
import Foundation
import GoogleMaps
import GooglePlaces
import CoreLocation
import SceneKit
import ARKit
class BaseNode : SCNNode {
    var title : String?
    var location : CLLocationCoordinate2D?
    var anchor : ARAnchor?
    var direction : String?
    
    init(_ title : String ,_ location : CLLocationCoordinate2D,_ direction  :String?) {
        super.init()
        self.title = title
        self.name = title
        self.location = location
         self.direction = direction ?? "Head"
        guard let current = LocationManager.sharedInstance.currentLocation else {
            return
        }
        let transform = CardinalDirection.getTransformGiven(currentLocation: current.coordinate, endLocation:self.location!,isSteps : false)
        self.transform = transform
        self.anchor = ARAnchor(transform: float4x4.init(transform))
         self.geometry = createBox()
        createTextBlock()
//        guard let direc = direction else {
//            return
//        }
       
       
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createTextBlock(){
        let textBlock = SCNText(string: "", extrusionDepth: 0.5)
        CardinalDirection.geoDecoding((self.location)!) { (placeName) in
            guard let name = placeName else {
                return
            }
            textBlock.string = name
        }
        textBlock.font.withSize(5.0)
        textBlock.flatness = -2.0
        textBlock.materials = getMaterials()
        let myTextNode = SCNNode(geometry: textBlock)
        myTextNode.position = SCNVector3(x: 0, y: 0, z: 0)
        myTextNode.orientation = SCNQuaternion(x: 0.1, y: 0, z: 0.5, w: 0)
        addChildNode(myTextNode)
    }
    
    func createBox()->SCNBox{
        let box = SCNBox(width: 2.0, height: 5.0, length: 1.0, chamferRadius: 0.0)
        box.materials = getMaterials()
        return box
    }
    func getMaterials()->[SCNMaterial]{
        let sphereMaterial = SCNMaterial()
//        if let direc = direction {
//        switch direc {
//        case "turn-left" :
//            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_invalid_left")
//        case  "uturn-left" :
//            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_uturn")
//        case  "turn-slight-right" :
//            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_turn_slight_right")
//        case  "turn-slight-left" :
//            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_turn_slight_left")
//        case  "turn-sharp-right" :
//            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_notificaiton_sharp_right")
//        case  "turn-sharp-left" :
//            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_new_name_sharp_left")
//        case  "turn-right" :
//            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_on_ramp_right")
//        case  "straight" :
//            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_new_name_straight")
//        case  "roundabout-right" :
//            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_roundabout_right")
//        case  "roundabout-left" :
//            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_roundabout_left")
//        case  "ramp-right" :
//            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_off_ramp_right")
//        case  "ramp-left" :
//            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_off_ramp_left")
//        case  "keep-right" :
//            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_continue_right")
//        case  "keep-left" :
//            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_invalid_left")
//        case  "fork-right" :
//            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_off_ramp_right")
//        case  "fork-left" :
//            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_fork_left")
//        case "South":
//            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_new_name_straight")
//        case "North":
//            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_new_name_straight")
//        default:
//            sphereMaterial.diffuse.contents = UIColor.red
//            break
//        }
//        }
           sphereMaterial.diffuse.contents = UIColor.red
        return [sphereMaterial]
    }
    
}

