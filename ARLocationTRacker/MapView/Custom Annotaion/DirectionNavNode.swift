//
//  DirectionNavNode.swift
//  ARLocationTRacker
//
//  Created by Sharanabasappa-Macmini on 11/10/17.
//  Copyright © 2017 ymedialabs. All rights reserved.
//
import UIKit
import Foundation
import GoogleMaps
import GooglePlaces
import CoreLocation
import SceneKit
import ARKit

class DirectionNavNode: SCNNode {
    var title : String?
    var location : CLLocationCoordinate2D?
    var anchor : ARAnchor?
    var direction : String?
    init(_ title : String ,_ location : CLLocationCoordinate2D,_ direction  :String?) {
        super.init()
        self.title = title
        self.name = title
        self.location = location
        self.direction = direction ?? "Head"
        guard let current = LocationManager.sharedInstance.currentLocation else {
            return
        }
        let transform = CardinalDirection.getTransformGiven(currentLocation: current.coordinate, endLocation:self.location!,isSteps : false)
        self.anchor = ARAnchor(transform: float4x4.init(transform))
       self.position.y = -10
        self.transform = transform
        createTextBlock()
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func createTextBlock(){
        let textBlock = SCNText(string: "", extrusionDepth: 0.5)
        CardinalDirection.geoDecoding((self.location)!) { (placeName) in
            guard let name = placeName else {
                return
            }
            textBlock.string = name
        }
        textBlock.font.withSize(5.0)
        textBlock.flatness = -2.0
        textBlock.firstMaterial?.diffuse.contents = UIColor.blue
        textBlock.alignmentMode = kCAAlignmentCenter
        let myTextNode = SCNNode(geometry: textBlock)
        myTextNode.position = SCNVector3(x: 0, y: 0, z: 0)
        myTextNode.orientation = SCNQuaternion(x: 0.1, y: 0, z: 0.5, w: 0)
        addChildNode(myTextNode)
    }
    
   
}
