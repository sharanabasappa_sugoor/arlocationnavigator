//
//  ViewController.swift
//  ARKitCompassRose
//
//  Created by Vasile Cotovanu on 25.07.17.
//  Copyright © 2017 vasile.ch. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import CoreLocation
struct PreferencesKeys {
    static let savedItems = "savedRegions"
}
class ViewController: UIViewController, ARSCNViewDelegate,LocationManagerDelegate {
    var LocationsList :[CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
    var isaddeed = true
    @IBOutlet var sceneView: ARSCNView!
    public var sceneNode: SCNNode?
    var smallDistanceAngle : Float?
    var neaestDestination : Double?
    
    @IBOutlet weak var directionsLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        LocationManager.sharedInstance.delegate = self
        LocationManager.sharedInstance.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        LocationManager.sharedInstance.locationManager.distanceFilter = 2
        LocationManager.sharedInstance.locationManager.requestWhenInUseAuthorization()
        self.navigationController?.navigationBar.isTranslucent = false
        sceneView.delegate = self
        // sceneView.showsStatistics = true
        let scene = SCNScene()
    
        sceneView.scene = scene
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        trackLocation()
        self.navigationController?.navigationBar.isHidden = true
        let defaults = UserDefaults.standard
        let placesData2 = defaults.object(forKey: "Places") as? NSData
        LocationsList.removeAll()
        smallDistanceAngle = nil
        neaestDestination = nil
       
        if ARWorldTrackingSessionConfiguration.isSupported {
        let configuration = ARWorldTrackingSessionConfiguration()
        // Align the real world on z(North-South) x(West-East) axis
        
        configuration.planeDetection = .horizontal
        configuration.worldAlignment = .gravityAndHeading
        sceneView.session.run(configuration, options: .resetTracking)
        }
        else if ARSessionConfiguration.isSupported  {
            let configuration = ARSessionConfiguration()
            // Align the real world on z(North-South) x(West-East) axis
            
          
         // configuration.worldAlignment = .gravityAndHeading
            sceneView.session.run(configuration, options: .resetTracking)
        }
        
        if let placesData2 = placesData2 {
            let placesArray = NSKeyedUnarchiver.unarchiveObject(with: placesData2 as Data) as? [CLLocation]
            if let placesArray = placesArray {
                for place in placesArray {
                    LocationsList.append(place.coordinate)
                }
                updateLocation()
                //isaddeed = false
            }
            //            updateLocation()
            //            isaddeed = false
        }
        
    }
    
    func trackLocation(){
        let defaults = UserDefaults.standard
        let placesData = defaults.object(forKey: "TrackLocation") as? NSData
        self.directionsLabel.text =  ""
        if let placesData = placesData {
            let trackInfo = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as? CLLocation
            if let trackInfo = trackInfo {
                guard let current = LocationManager.sharedInstance.currentLocation else {
                    return
                    
                }
                ViewController.geoDecoding(trackInfo.coordinate, completionHandler: { (locname) in
                    self.directionsLabel.text = locname ?? ""
//                    DispatchQueue.main.async {
//                        self.pushTransition(label: self.directionsLabel, animationType:kCATransitionFromRight, animationDuration: 5.0)
//                    }
                    
                    
                })
                neaestDestination = BearAngle.distanceTwopoints(map1: current.coordinate, map2: trackInfo.coordinate)
                smallDistanceAngle =  BearAngle.bearingBetween(startLocation:  current.coordinate, endLocation: trackInfo.coordinate)
            }
        }
        
        
    }
    func didSetLocation(location: CLLocation?){
        if isaddeed {
            isaddeed = false
            updateLocation()
        }
        else{
            guard let curLoc  = LocationManager.sharedInstance.currentLocation else  {
                return
            }
            
            
            var index = 0
            for locs in LocationsList {
                let sphereNode = sceneNode?.childNode(withName: String(index), recursively: true)
                guard let childnodes = sphereNode?.childNodes else {
                    return
                }
                for textnode in childnodes {
                    guard let node2 = textnode.geometry as? SCNText else{
                        return
                    }
                    trackLocation()
                    //                    if smallDistanceAngle == nil {
                    //                        neaestDestination = BearAngle.distanceTwopoints(map1:  curLoc.coordinate, map2: LocationsList[index])
                    //                        smallDistanceAngle =  BearAngle.bearingBetween(startLocation: curLoc.coordinate, endLocation: LocationsList[index])
                    //                    }
                    //                    else
                    //                    {  let newDestination = BearAngle.distanceTwopoints(map1:  curLoc.coordinate, map2: LocationsList[index])
                    //                        let newAngle =  BearAngle.bearingBetween(startLocation: curLoc.coordinate, endLocation: LocationsList[index])
                    //                        if newDestination < neaestDestination! {
                    //                            neaestDestination = newDestination
                    //                            smallDistanceAngle = newAngle
                    //                        }
                    //
                    //                    }
                    
                    SCNTransaction.begin()
                    //node2.string  =
                    CardinalDirection.geoDecoding(LocationsList[index]) { (locname) in
                        guard let name = locname else {
                            return
                        }
                        node2.string = name
                    }
                    
                    //node2.string =  String(Double(round(100000*(BearAngle.distanceTwopoints(map1: curLoc.coordinate, map2: LocationsList[index])/1000))/100000) )
                    index = index + 1
                    SCNTransaction.commit()
                }
                
                
            }
        }
        
    }
    func updateLocation(){
        let constraint = SCNLookAtConstraint(target: sceneView.pointOfView)
        
        // Keep the rotation on the horizon
        constraint.isGimbalLockEnabled = true
        
        // Slow the constraint down a bit
        constraint.influenceFactor = 0.01
        
        // Finally add the constraint to the node
        
        let scene = SCNScene()
        scene.rootNode.constraints = [constraint]
        var index = 0
        for direction in LocationsList {
            guard let loc = LocationManager.sharedInstance.currentLocation else {
                
                return
            }
            trackLocation()
            //            if smallDistanceAngle == nil {
            //                neaestDestination = BearAngle.distanceTwopoints(map1:  (LocationManager.sharedInstance.currentLocation?.coordinate)!, map2: LocationsList[index])
            //                smallDistanceAngle =  BearAngle.bearingBetween(startLocation: (LocationManager.sharedInstance.currentLocation?.coordinate)!, endLocation: LocationsList[index])
            //            }
            //            else
            //            {  let newDestination = BearAngle.distanceTwopoints(map1:  (LocationManager.sharedInstance.currentLocation?.coordinate)!, map2: LocationsList[index])
            //                let newAngle =   BearAngle.bearingBetween(startLocation: (LocationManager.sharedInstance.currentLocation?.coordinate)!, endLocation: LocationsList[index])
            //                if newDestination < neaestDestination! {
            //                    neaestDestination = newDestination
            //                    smallDistanceAngle = newAngle
            //                }
            //
            //            }
            let direction = CardinalDirection(name: "a", angle: 0.0)
            scene.rootNode.addChildNode(direction.sphere(currentLocation: loc.coordinate, endloc: LocationsList[index],locname:String(index)))
            
            //scene.rootNode.addChildNode(direction.text())
            // sceneView.session.add(anchor: )
            //scene.rootNode.addChildNode(direction.text())
            index = index + 1;
        }
        sceneNode = scene.rootNode
        // sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        sceneView.scene = scene
        //sceneView.projectPoint()
    }
    func getCurrentcursorpos()-> SCNVector3?{
        guard let currPos = sceneView.pointOfView else {
            return nil
        }
        return sceneView.scene.rootNode.convertPosition(currPos.worldPosition, to: sceneNode)
    }
    func didUpdateHeading(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading){
        if newHeading.headingAccuracy < 0 {
           // debugPrint(newHeading.trueHeading)
            addDirectionPlane(direction:newHeading.trueHeading)
        }
        else {
            addDirectionPlane(direction:newHeading.magneticHeading)
           // debugPrint(newHeading.magneticHeading)
        }
        
        
    }
    
    func addDirectionPlane(direction:CLLocationDirection){
        let arrowPlane = sceneView.pointOfView?.childNode(withName: "arrow", recursively: false)
        guard let angle = smallDistanceAngle else {
            return
        }
        guard let planes = arrowPlane?.geometry as? SCNBox else {
            let plane = SCNBox(width: 10.0, height: 20.0, length: 5.0, chamferRadius: 0.0)
            plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "up")
            let smallDistanceQuardant  = Int(ceil(smallDistanceAngle! / 90.0))
            let currentQuartdant  = Int(ceil(direction / 90.0))
            
            var lowerRange =  (Int(smallDistanceAngle! - 5))...Int(smallDistanceAngle!)
            var upperRange = (Int(smallDistanceAngle! + 1))...(Int(smallDistanceAngle! + 5))
            if (Int(smallDistanceAngle! - 5)) <= 360 && (Int(smallDistanceAngle! + 5)) >= 360 {
                // centerVal = 360
                lowerRange =  (Int(smallDistanceAngle! - 5))...360
                upperRange = (0)...(Int(smallDistanceAngle! + 5))
            }
            if lowerRange.contains(Int(direction)) || upperRange.contains(Int(direction)) {
                plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "up")
            }
            else if currentQuartdant == 4 {
                if smallDistanceQuardant == 1 || smallDistanceQuardant == 2 {
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
                }
                else if smallDistanceQuardant == 3 {
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
                }
                else if Int(direction) < lowerRange.lowerBound {
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
                }
                else if Int(direction) <  upperRange.upperBound {
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
                }
            }
            else if currentQuartdant == 3 {
                if smallDistanceQuardant == 4 || smallDistanceQuardant == 1{
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
                }
                else if smallDistanceQuardant == 2 {
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
                }else if Int(direction) < lowerRange.lowerBound {
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
                }
                else if Int(direction) <  upperRange.upperBound {
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
                }
            }
            else if currentQuartdant == 2{
                if smallDistanceQuardant == 1 || smallDistanceQuardant == 4{
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
                }
                else if smallDistanceQuardant == 3 {
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
                } else if Int(direction) < lowerRange.lowerBound {
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
                }
                else if Int(direction) <  upperRange.upperBound {
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
                }
            }
            else if currentQuartdant == 1{
                if smallDistanceQuardant == 4 || smallDistanceQuardant == 3{
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
                }
                else if smallDistanceQuardant == 2 {
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
                }else if Int(direction) < lowerRange.lowerBound {
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
                }
                else if Int(direction) <  upperRange.upperBound {
                    plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
                }
            }
            
            let directionNode = SCNNode()
            
            directionNode.name = "arrow"
            directionNode.geometry = plane
            //            directionNode.position = getCurrentcursorpos()!
            directionNode.position = SCNVector3Make(0, 0, -100.2)
            
            sceneView.pointOfView?.addChildNode(directionNode)
            //            sceneNode?.addChildNode(directionNode)
            return
        }
        
        let smallDistanceQuardant  = Int(ceil(smallDistanceAngle! / 90.0))
        let currentQuartdant  = Int(ceil(direction / 90.0))
        var centerVal = smallDistanceAngle
        var lowerRange =  (Int(smallDistanceAngle! - 5))...Int(smallDistanceAngle!)
        var upperRange = (Int(smallDistanceAngle! + 1))...(Int(smallDistanceAngle! + 5))
        if (Int(smallDistanceAngle! - 5)) <= 360 && (Int(smallDistanceAngle! + 5)) >= 360 {
            // centerVal = 360
            lowerRange =  (Int(smallDistanceAngle! - 5))...360
            upperRange = (0)...(Int(smallDistanceAngle! + 5))
        }
        
        if lowerRange.contains(Int(direction)) || upperRange.contains(Int(direction)) {
            planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "up")
        }
        else if currentQuartdant == 4 {
            if smallDistanceQuardant == 1 || smallDistanceQuardant == 2{
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
            }
            else if smallDistanceQuardant == 3 {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
            }
            else if Int(direction) < lowerRange.lowerBound {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
            }
            else if Int(direction) <  upperRange.upperBound {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
            }
        }
        else if currentQuartdant == 3 {
            if smallDistanceQuardant == 4 || smallDistanceQuardant == 1{
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
            }
            else if smallDistanceQuardant == 2 {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
            } else if Int(direction) < lowerRange.lowerBound {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
            }
            else if Int(direction) <  upperRange.upperBound {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
            }
        }
        else if currentQuartdant == 2{
            if smallDistanceQuardant == 1 || smallDistanceQuardant == 4{
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
            }
            else if smallDistanceQuardant == 3 {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
            }
            else if Int(direction) < lowerRange.lowerBound {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
            }
            else if Int(direction) <  upperRange.upperBound {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
            }
        }
        else if currentQuartdant == 1{
            if smallDistanceQuardant == 4 || smallDistanceQuardant == 3{
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
            }
            else if smallDistanceQuardant == 2 {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
            }
            else if Int(direction) < lowerRange.lowerBound {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
            }
            else if Int(direction) <  upperRange.upperBound {
                planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
            }
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        LocationManager.sharedInstance.startUpdating()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    // MARK: - ARSCNViewDelegate
    
    public func renderer(_ renderer: SCNSceneRenderer, didRenderScene scene: SCNScene, atTime time: TimeInterval) {
        
        //updateLocation()
    }
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
        // updateLocation()
        return nil
    }
    
    public func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor){
        
    }
    public func renderer(_ renderer: SCNSceneRenderer, willUpdate node: SCNNode, for anchor: ARAnchor){
        
    }
    
    public func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor)
    {
        
    }
    public func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        
    }
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
    func session(_: ARSession, didUpdate: ARFrame){
        
    }
    func session(_: ARSession, didAdd: [ARAnchor]){
        
    }
    func session(_: ARSession, didUpdate: [ARAnchor]) {
        
    }
    
    @IBAction func addLocations(_ sender: UIButton) {
        sceneView.scene.rootNode.enumerateChildNodes { (node, stop) -> Void in
            node.removeFromParentNode()
        }
        LocationsList.removeAll()
        smallDistanceAngle = nil
        neaestDestination = nil
        isaddeed = true
        let add = ADDLocationVC(nibName: "ADDLocationVC", bundle: nil)
        self.navigationController?.pushViewController(add, animated: true)
    }
    
    @IBAction func clearNodes(_ sender: Any) {
        self.directionsLabel.text = ""
        guard let node  = sceneNode else {
            return
        }
        sceneView.session.pause()
        sceneView.scene.rootNode.enumerateChildNodes { (node, stop) -> Void in
            node.removeFromParentNode()
        }
        smallDistanceAngle = nil
        neaestDestination = nil
        isaddeed = true
        sceneView.session.run(sceneView.session.configuration!)
        let defaults = UserDefaults.standard
        //let placesData1 = NSKeyedArchiver.archivedData(withRootObject: [])
        defaults.set(nil, forKey: "Places")
        defaults.set(nil, forKey: "TrackLocation")
        LocationsList.removeAll()
        UserDefaults.standard.synchronize()
        updateLocation()
    }
}
extension ViewController {
    class func geoDecoding(_ geoLoc:CLLocationCoordinate2D,completionHandler handler:@escaping completeHandler) {
        let location = CLLocation(latitude: geoLoc.latitude, longitude: geoLoc.longitude)
        CLGeocoder().reverseGeocodeLocation(location) { (placeMark, error) in
            guard let places  = placeMark else {
                return
            }
            if error != nil {
                print(error)
                return
            }
            else if places.count > 0 {
                let pm = places[0]
                var subname = ""
                var local = ""
                if let sub = pm.subLocality {
                    subname = sub
                }
                if let local1 = pm.locality {
                    local = local1
                }
                //debugPrint(pm.locality)
                //debugPrint(pm.name)
                
                //debugPrint(pm.subLocality)
                
                handler(subname + " " + local)
                //debugPrint(pm.thoroughfare)
                //debugPrint(pm.subLocality)
            }
        }
        
    }
    func pushTransition(label: UILabel, animationType subType: String, animationDuration duration: CGFloat) {
        
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        animation.repeatCount = HUGE
        animation.autoreverses = true
        animation.type = kCATransitionPush
        animation.subtype = subType
        animation.duration = CFTimeInterval(duration)
        label.layer.add(animation, forKey: "kCATransitionPush")
        
    }
}
//extension ViewController : SCNSceneRendererDelegate{
//    public func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval){
//        //        updateLocation()
//
//        //        SCNTransaction.commit()
//    }
//    public func renderer(_ renderer: SCNSceneRenderer, didApplyAnimationsAtTime time: TimeInterval){
//
//    }
//    public func renderer(_ renderer: SCNSceneRenderer, didApplyConstraintsAtTime time: TimeInterval){
//
//    }
//
//
//}
//extension ViewController {
//
//}

