//
//  CustomAnnotationView.swift
//  MAP-POC
//
//  Created by yml on 23/06/17.
//  Copyright © 2017 yml. All rights reserved.
//

import UIKit
import MapKit

class CustomAnnotationView: MKPinAnnotationView{
    var myview : CustomAnnotView?
    var locationCordinate : CLLocationCoordinate2D?
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func customView(){
         myview = Bundle.main.loadNibNamed("CustomAnnotView", owner: nil, options: nil)?.first as? CustomAnnotView
       // self.locationCordinate = locationCordinate
     
       // self.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height:  UIScreen.main.bounds.height)
        
        myview?.frame = CGRect(x: 0.0, y: 0.0, width: 200, height:  100)
        //addBtn()
        
        self.detailCalloutAccessoryView = myview
        
    }
    func addBtn(){
        let button = UIButton(frame: CGRect(x: frame.size.width / 2, y: frame.size.height - 10, width: 100, height: 50))
        button.setTitle("hel", for: .normal)
        button.backgroundColor = UIColor.red
        button.addTarget(self, action: #selector(btntapped), for: .touchUpInside)
        self.rightCalloutAccessoryView = button
    }
    @objc func btntapped(){
        print("btn")
    }
}
