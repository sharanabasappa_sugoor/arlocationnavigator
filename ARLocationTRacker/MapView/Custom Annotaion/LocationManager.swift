//
//  LocationManager.swift
//  SayBubble
//
//  Created by Y MEDIA LABS on 18/08/16.
//  Copyright © 2016 Y MEDIA LABS. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

@objc protocol LocationManagerDelegate : class {
    @objc optional func didSetLocation(location: CLLocation?)
    @objc optional func didUpdateHeading(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading)
    @objc optional func didFailWithError(error: NSError)
    @objc optional func didEnterRegion(_ manager: CLLocationManager, didEnterRegion region: CLRegion)
    @objc optional func didExitRegion(_ manager: CLLocationManager, didExitRegion region: CLRegion)
}

/// Class to fetch the location details of the user.
class LocationManager : NSObject, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    
    /// Stores the current location of the user.
    var currentLocation : CLLocation?
    
    /// Stores the authrization status of location.
    var currentStatus : CLAuthorizationStatus = .notDetermined
    
    weak var delegate : LocationManagerDelegate?
    
    /// A shared instance for the LocationManager class.
    static let sharedInstance = LocationManager()
    
    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.distanceFilter = 1
        
        
    }
    
    /// Start updating the location.
    func startUpdating() {
        locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
    }
    
    /// Stop updating the location.
    func stopUpdating() {
        locationManager.stopUpdatingLocation()
        locationManager.stopUpdatingHeading()
    }
    
    //MARK:- CLLocationManagerDelegate methods
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        for region in locationManager.monitoredRegions{
            locationManager.requestState(for: region)
        }
        //        if let _ = currentLocation {
        //
        //            self.delegate?.didSetLocation?(location: currentLocation)
        //            return
        //        }
        //let loca = CLLocation(latitude: 12.960306, longitude:  77.643863)
        currentLocation = locations.last
       // currentLocation = loca
        self.delegate?.didSetLocation?(location: currentLocation)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        currentLocation = nil
        
        self.delegate?.didFailWithError?(error: error as NSError)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus){
        currentStatus = status
        switch status {
        case .notDetermined,.restricted, .denied:
            locationManager.requestWhenInUseAuthorization()
        case .authorizedWhenInUse:
            locationManager.startMonitoringSignificantLocationChanges()
        case .authorizedAlways :
            break
            //         let geo = Geotification(coordinate: (manager.location?.coordinate)!, radius: 100, identifier: NSUUID().uuidString, note: "", eventType: .onEntry)
        //                self.AddRegion(geotification: geo)
        default:
            startUpdating()
            break
        }
    }
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion){
        if state == .inside {
            self.delegate?.didEnterRegion?(manager, didEnterRegion: region)
        }
        
        
    }
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Monitoring failed for region with identifier: \(region!.identifier)")
        
    }
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        print("Started Monitoring Region: \(region.identifier)")
        locationManager.requestState(for: region)
    }
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if region is CLCircularRegion {
            self.delegate?.didEnterRegion!(manager, didEnterRegion: region)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        
        if region is CLCircularRegion {
            self.delegate?.didExitRegion!(manager, didExitRegion: region)
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        self.delegate?.didUpdateHeading!(manager, didUpdateHeading: newHeading)
        //        debugPrint("1:-",newHeading.headingAccuracy)
        // debugPrint("2:-",newHeading.magneticHeading)
        //        debugPrint("3:-",newHeading.trueHeading)
        //        debugPrint("4:-",newHeading.x,newHeading.y,newHeading.z)
    }
    
    func AddRegion(geotification:Geotification) {
        if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self){
            
            let geofenceRegionCenter = CLLocationCoordinate2DMake( geotification.coordinate.latitude,geotification.coordinate.longitude)
            
            let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter, radius: geotification.radius, identifier: NSUUID().uuidString)
            
            geofenceRegion.notifyOnExit = true;
            geofenceRegion.notifyOnEntry = true;
            
            self.locationManager.startMonitoring(for: geofenceRegion)
            
        }
        
    }
}
