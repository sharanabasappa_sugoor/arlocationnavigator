//
//  CustomAnnotView.swift
//  ARKitCompassRose
//
//  Created by Sharanabasappa-Macmini on 13/09/17.
//  Copyright © 2017 vasile.ch. All rights reserved.
//

import UIKit
import  CoreLocation
@objc protocol MapAnnotationActions : class {
    @objc optional func taskStartAct()
    @objc optional func taskStopAct()
    @objc optional func routeDirections()
}
typealias deleteAnnotaion = ((_ annot:CustomAnnotView) -> ())
typealias navigateAnnotation = ((_ annot:CustomAnnotView) -> ())
class CustomAnnotView: UIView {
    var locationCordinate : CLLocationCoordinate2D?
    var navigate:navigateAnnotation?
    var delete:deleteAnnotaion?
    
    
    override func awakeFromNib() {
        
    }
    
    weak var delegate : MapAnnotationActions?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    
    @IBAction func startAct(_ sender: Any) {
        navigate!(self)
        // self.delegate?.taskStartAct?()
        // self.window?.rootViewController?.showAlert(withTitle: "MoPaSTA", message: "Task has been Started")
    }
    
    
    @IBAction func stopAct(_ sender: Any) {
        delete!(self)
        //self.delegate?.taskStopAct?()
        // self.window?.rootViewController?.showAlert(withTitle: "MoPaSTA", message: "Task has been Stopped")
    }
    
    @IBAction func routeDirections(_ sender: Any) {
        self.delegate?.routeDirections?()
    }
    
    
}
