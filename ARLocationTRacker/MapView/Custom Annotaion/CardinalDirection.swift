//
//  CardinalDirection.swift
//  ARKitCompassRose
//
//  Created by Vasile Cotovanu on 25.07.17.
//  Copyright © 2017 vasile.ch. All rights reserved.
//

import UIKit
import SceneKit
import CoreLocation
import simd
import GLKit
import  GoogleMaps
import GooglePlaces
typealias completeHandler = ((_ locName : String?) -> ())

class CardinalDirection: NSObject {
    var name: String
    var angle: Float
    
    init(name: String, angle: Float) {
        self.name = name
        self.angle = angle
        
        super.init()
    }
    
    func sphere(currentLocation: CLLocationCoordinate2D,endloc : CLLocationCoordinate2D,locname:String,isSteps : Bool,direction:String) -> SCNNode {
           let str = "Loading..."
            //String(Double(round(100000*(BearAngle.distanceTwopoints(map1: currentLocation, map2: endloc)/1000))/100000) )
        let distance = BearAngle.distanceTwopoints(map1: currentLocation, map2: endloc)
        var radius = 5
        if distance  <= 100 {
            radius = 2
        }
        let sphere = SCNBox(width: 2.0, height: 5.0, length: 1.0, chamferRadius: 0.0)
            //SCNSphere(radius: CGFloat(radius))
        
        let sphereMaterial = SCNMaterial()
         sphereMaterial.diffuse.contents = UIColor.red

        
      
        let textBlock = SCNText(string: "\(str)", extrusionDepth: 1)
        CardinalDirection.geoDecoding(endloc) { (locname) in
            guard let name = locname else {
                return
            }
            textBlock.string = name
        }
        textBlock.font.withSize(5.0)
        textBlock.flatness = -2.0
        textBlock.firstMaterial?.diffuse.contents = UIColor.blue
        textBlock.alignmentMode = kCAAlignmentCenter
        textBlock.font = UIFont(name: "Helvatica", size: 1.0)
        
        let node1 = SCNNode(geometry: textBlock)
        
        //node1.name = locname //String(endloc.coordinate.latitude)+String(endloc.coordinate.longitude)
        node1.scale = SCNVector3Make( 0.2, 0.2, 0.2);
        node1.position = SCNVector3(-5, -7,10)
        let paresntnode = SCNNode(geometry: sphere)
        
        paresntnode.name = isSteps ? "s" + locname : locname
        
        paresntnode.geometry?.materials = [sphereMaterial]
//        paresntnode.o
        paresntnode.transform = CardinalDirection.getTransformGiven(currentLocation: currentLocation, endLocation: endloc,isSteps : isSteps)
             paresntnode.addChildNode(node1)
        //transform(rotationY: GLKMathDegreesToRadians(angle), distance: 100)
        
        return paresntnode
    }
    
    func addNavigationNode(currentLocation: CLLocationCoordinate2D,endloc : CLLocationCoordinate2D,locname:String,isSteps : Bool,direction:String) -> SCNNode{
        let str = "Loading..."
        //String(Double(round(100000*(BearAngle.distanceTwopoints(map1: currentLocation, map2: endloc)/1000))/100000) )
        let distance = BearAngle.distanceTwopoints(map1: currentLocation, map2: endloc)
        var radius = 5
        if distance  <= 100 {
            radius = 2
        }
       // var directionPlane =  SCNBox(width: 2.0, height: 2.0, length: 0.01, chamferRadius: 0.0)//SCNFloor()//SCNPlane(width: 2, height: 2)
//        directionPlane.width = 2
//        directionPlane.length = 0
        //(width: 2.0, height: 5.0, length: 1.0, chamferRadius: 0.0)
        //SCNSphere(radius: CGFloat(radius))
        
            //directionPlane
            // SCNBox(width: 2.0, height: 5.0, length: 1.0, chamferRadius: 0.0)
        //SCNSphere(radius: CGFloat(radius))
       //  directionNode.transform = SCNMatrix4MakeRotation(-Float.pi / 3, 1.0, 0, 0)
        //debugPrint(direction)
        let sphereMaterial = SCNMaterial()
     
        switch direction {
        case "turn-left" :
             sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_invalid_left")
        case  "uturn-left" :
              sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_uturn")
        case  "turn-slight-right" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_turn_slight_right")
        case  "turn-slight-left" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_turn_slight_left")
        case  "turn-sharp-right" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_notificaiton_sharp_right")
        case  "turn-sharp-left" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_new_name_sharp_left")
        case  "turn-right" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_on_ramp_right")
        case  "straight" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_new_name_straight")
        case  "roundabout-right" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_roundabout_right")
        case  "roundabout-left" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_roundabout_left")
        case  "ramp-right" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_off_ramp_right")
        case  "ramp-left" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_off_ramp_left")
        case  "keep-right" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_continue_right")
        case  "keep-left" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_invalid_left")
        case  "fork-right" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_off_ramp_right")
        case  "fork-left" :
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_fork_left")
        case "South":
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_new_name_straight")
        case "North":
            sphereMaterial.diffuse.contents = #imageLiteral(resourceName: "direction_new_name_straight")
        default:
            sphereMaterial.diffuse.contents = UIColor.red
            break
        }
        
     
        //  let str =  String(Double(round(100000*(BearAngle.distanceTwopoints(map1: currentLocation, map2: endloc)/1000))/100000) )
        
        
        let textBlock = SCNText(string: "\(str)", extrusionDepth: 1)
       
        CardinalDirection.geoDecoding(endloc) { (locname) in
            guard let name = locname else {
                return
            }
            textBlock.string = name
        }
       
        textBlock.font.withSize(5.0)
        textBlock.flatness = -2.0
        textBlock.firstMaterial?.diffuse.contents = UIColor.blue
        textBlock.alignmentMode = kCAAlignmentCenter
        textBlock.font = UIFont(name: "Helvatica", size: 1.0)
        
        let textnode = SCNNode(geometry: textBlock)
        
        textnode.scale = SCNVector3Make( 0.1, 0.1, 0.1);
        //let paresntnode = SCNNode(geometry: directionPlane)
     
           let paresntnode = SCNNode()
        paresntnode.name =  locname
       
        textnode.position = SCNVector3(0,0,0)
       // paresntnode.geometry?.materials = [sphereMaterial]
     
        //  paresntnode.transform = SCNMatrix4MakeRotation(-Float.pi / 2 , 1.0, 0, 0)
        paresntnode.transform = CardinalDirection.getTransformGiven(currentLocation: currentLocation, endLocation: endloc,isSteps : isSteps)
         paresntnode.position.y = -10
                //paresntnode.transform  =  SCNMatrix4MakeRotation(-Float.pi / 4, 1.0, 0, 0)
        paresntnode.addChildNode(textnode)
        
        return paresntnode
    }
    
    func text() -> SCNNode {
        let textBlock = SCNText(string: "\(name) \(angle)°", extrusionDepth: 0.5)
        textBlock.firstMaterial?.diffuse.contents = UIColor.blue
        
        let node = SCNNode(geometry: textBlock)
        node.transform = CardinalDirection.transform(rotationY: GLKMathDegreesToRadians(angle), distance: 100)
        
        return node
    }
    
    static func transform(rotationY: Float, distance: Int) -> SCNMatrix4 {
        
        // Translate first on -z direction
        let translation = SCNMatrix4MakeTranslation(0, 0, Float(-distance))
        // Rotate (yaw) around y axis
        let rotation = SCNMatrix4MakeRotation(-1 * rotationY, 0, 1, 0)
        
        // Final transformation: TxR
        let transform = SCNMatrix4Mult(translation, rotation)
        
        return transform
    }
  
    class func getTransformGiven(currentLocation: CLLocationCoordinate2D,endLocation : CLLocationCoordinate2D,isSteps : Bool) -> SCNMatrix4 {
        
        let bearing = BearAngle.bearingBetween(
            startLocation: currentLocation,
            endLocation: endLocation
        )
        debugPrint("heading : \(GMSGeometryHeading(currentLocation, endLocation))\n")
        debugPrint("bearing:\(bearing)\n")
        let distance = BearAngle.distanceTwopoints(map1: currentLocation, map2: endLocation) 
    //let distance = BearAngle.distanceTwopoints(map1: currentLocation, map2: endLocation)
      
        let translation = SCNMatrix4MakeTranslation(0, 0, Float(-distance))
        // Rotate (yaw) around y axis
        let rotation = SCNMatrix4MakeRotation(-1 * GLKMathDegreesToRadians(Float(GMSGeometryHeading(currentLocation, endLocation))), 0, 1, 0)
  
        let transform = SCNMatrix4Mult(translation, rotation)
    
        return transform
    }
    class func geoDecoding(_ geoLoc:CLLocationCoordinate2D,completionHandler handler:@escaping completeHandler) {
        let location = CLLocation(latitude: geoLoc.latitude, longitude: geoLoc.longitude)
        CLGeocoder().reverseGeocodeLocation(location) { (placeMark, error) in
            guard let places  = placeMark else {
                return
            }
            if error != nil {
                print(error)
                return
            }
            else if places.count > 0 {
                let pm = places[0]
               var subname = ""
                var local = ""
                if let sub = pm.subLocality {
                    subname = sub
                }
                if let local1 = pm.locality {
                    local = local1
                }
                debugPrint("locality :- \(pm.locality) ")
                debugPrint("name :- \(pm.name)")
                
                debugPrint("subLocality :- \(pm.subLocality)")
                debugPrint("thoroughfare :- \(pm.thoroughfare)")
                debugPrint("subThoroughfare :- \(pm.subThoroughfare)")
                //handler(subname + " " + local)
                handler(pm.thoroughfare ?? "Loading...")
            }
        }
        
    }
}

extension CardinalDirection {
//CardinalDirection(name: "N", angle: 0),
//    CardinalDirection(name: "NE", angle: 45),
//    CardinalDirection(name: "E", angle: 90),
//    CardinalDirection(name: "SE", angle: 135),
//    CardinalDirection(name: "S", angle: 180),
    //CardinalDirection(name: "SW", angle: 225),
    // CardinalDirection(name: "W", angle: 270),
    //CardinalDirection(name: "NW", angle: 315),
//    static let defaults: [CardinalDirection] = {
//        return[
//            CardinalDirection(name: "N", angle: BearAngle.bearingBetween(
//                startLocation:LocationManager.sharedInstance.currentLocation.cordinate,
//                endLocation:LocationsList[0]
//            )),
//            CardinalDirection(name: "NE", angle:  BearAngle.bearingBetween(
//                startLocation:LocationManager.sharedInstance.currentLocation!,
//                endLocation:LocationsList[1]
//            )),
//            CardinalDirection(name: "E", angle:  BearAngle.bearingBetween(
//                startLocation:LocationManager.sharedInstance.currentLocation!,
//                endLocation:LocationsList[2]
//            )),
//            CardinalDirection(name: "SE", angle:  BearAngle.bearingBetween(
//                startLocation:LocationManager.sharedInstance.currentLocation!,
//                endLocation:LocationsList[3]
//            )),
//            CardinalDirection(name: "W", angle:  BearAngle.bearingBetween(
//                startLocation:LocationManager.sharedInstance.currentLocation!,
//                endLocation:LocationsList[4]
//            )),
//
            //CardinalDirection(name: "SW", angle: 225),
           // CardinalDirection(name: "W", angle: 270),
            //CardinalDirection(name: "NW", angle: 315),
//        ]
//    }()
}




