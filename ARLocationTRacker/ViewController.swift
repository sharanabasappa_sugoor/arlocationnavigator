//
//  ViewController.swift
//  ARKitCompassRose
//
//  Created by Vasile Cotovanu on 25.07.17.
//  Copyright © 2017 vasile.ch. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import Accelerate
struct PreferencesKeys {
    static let savedItems = "savedRegions"
}
class DirectionNodes {
    static let  sharedLocation = DirectionNodes()
    var fStartNode : SCNNode?
    var fendNode : SCNNode?
    var SecStartnode :SCNNode?
    var SecEndnode :SCNNode?
    func isStartNodesAdded() ->  Bool{
        return fStartNode != nil && fendNode != nil
    }
    func isSecondNodesAdded() -> Bool{
        return SecStartnode != nil && SecEndnode != nil
    }
    
}
class ViewController: UIViewController, ARSCNViewDelegate,LocationManagerDelegate {
    var lastPath : GMSPath?
    var LocationsList :[CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
    var isaddeed = true
    @IBOutlet var sceneView: ARSCNView!
    public var sceneNode: SCNNode?
    var smallDistanceAngle : Float?
    var neaestDestination : Double?
    var directionNodes : [SCNNode] = [SCNNode]()
    
    var endNode : SCNNode? = nil {
        didSet{
            if endNode != nil && directionNodes.last != nil{
                let startVector = SCNVector3.positionFromTransform((directionNodes.last?.worldTransform)!)
                let endVector = SCNVector3.positionFromTransform((endNode?.worldTransform)!)
                let lineNode =  CylinderLine(parent:directionNodes.last!,destNode:SCNNode(), v1: startVector, v2: endVector, radius: 0.2, radSegmentCount: 0, color: .green,startDirection: "Head",endDirection : "Head")
                self.sceneView.scene.rootNode.addChildNode(lineNode)
            }
        }
    }
    var angleList : [Float] = [Float]()
    fileprivate lazy var lines: [Line] = []
    fileprivate var currentLine: Line?
    fileprivate lazy var unit: DistanceUnit = .centimeter
    @IBOutlet weak var directionsLabel: UILabel!
    
    //stepsList
    var stepsList : [Steps] = [Steps]()
    override func viewDidLoad() {
        super.viewDidLoad()
        LocationManager.sharedInstance.delegate = self
        //        LocationManager.sharedInstance.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        //        LocationManager.sharedInstance.locationManager.distanceFilter = 2
        LocationManager.sharedInstance.locationManager.requestWhenInUseAuthorization()
        self.navigationController?.navigationBar.isTranslucent = false
        sceneView.delegate = self
        //sceneView.scene.rootNode.camera?.usesOrthographicProjection = true
        // sceneView.showsStatistics = true
        let scene = SCNScene()
        
        sceneView.scene = scene
        //        sceneView.autoenablesDefaultLighting = true
        //        sceneView.automaticallyUpdatesLighting = true
        sceneView.antialiasingMode = .multisampling4X
        //sceneView.allowsCameraControl = true
        
        sceneView.debugOptions = [ARSCNDebugOptions.showWorldOrigin]
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
        let defaults = UserDefaults.standard
        let placesData2 = defaults.object(forKey: "Places") as? NSData
        LocationsList.removeAll()
        smallDistanceAngle = nil
        neaestDestination = nil
        
        if ARConfiguration.isSupported {
            //            if ARFaceTrackingConfiguration.isSupported {
            //                let configuration = ARFaceTrackingConfiguration()
            //                // Align the real world on z(North-South) x(West-East) axis
            //
            //
            //                configuration.worldAlignment = .gravityAndHeading
            //                sceneView.session.run(configuration, options: [.resetTracking,.removeExistingAnchors])
            //            }
            //            else
            if ARWorldTrackingConfiguration.isSupported {
                let configuration = ARWorldTrackingConfiguration()
                // Align the real world on z(North-South) x(West-East) axis
                
                configuration.planeDetection = .horizontal
                configuration.worldAlignment = .gravityAndHeading
                sceneView.session.run(configuration, options: [.resetTracking,.removeExistingAnchors])
                // sceneView.debugOptions =  [ARSCNDebugOptions.showWorldOrigin]
            }
            else if AROrientationTrackingConfiguration.isSupported {
                let configuration = AROrientationTrackingConfiguration()
                // Align the real world on z(North-South) x(West-East) axis
                
                configuration.worldAlignment = .gravityAndHeading
                sceneView.session.run(configuration, options: [.resetTracking,.removeExistingAnchors])
            }
        }
        else{
            showAlert("AR Navigation is Not Supported in this device")
        }
        LocationManager.sharedInstance.startUpdating()
        if let placesData2 = placesData2 {
            let placesArray = NSKeyedUnarchiver.unarchiveObject(with: placesData2 as Data) as? [CLLocation]
            if let placesArray = placesArray {
                for place in placesArray {
                    LocationsList.append(place.coordinate)
                }
                updateLocation()
                //                 trackLocation()
                //isaddeed = false
            }
            //            updateLocation()
            //            isaddeed = false
        }
        
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //addNaviationControlPoints()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        LocationManager.sharedInstance.stopUpdating()
        // Pause the view's session
        sceneView.session.pause()
    }
    //MARK:- Track Location
    func trackLocation() {
        let defaults = UserDefaults.standard
        let placesData = defaults.object(forKey: "TrackLocation") as? NSData
        
        ///track Location
        if let placesData = placesData {
            let trackInfo = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as? CLLocation
            if let trackInfo = trackInfo {
                
                if let current = LocationManager.sharedInstance.currentLocation  {
                    
                    ViewController.geoDecoding(trackInfo.coordinate, completionHandler: { (locname) in
                        
                        if   self.directionsLabel.text != locname {
                            
                            self.directionsLabel.text = locname ?? "Loading..."
                        }
                        
                        debugPrint("Direction : \(trackInfo.course)")
                    })
                    if neaestDestination == nil && smallDistanceAngle == nil {
                        neaestDestination = BearAngle.distanceTwopoints(map1: current.coordinate, map2: trackInfo.coordinate)
                        smallDistanceAngle =  BearAngle.bearingBetween(startLocation:  current.coordinate, endLocation: trackInfo.coordinate)
                        
                    }
                    if  let _  = LocationManager.sharedInstance.currentLocation   {
                        var index = 0
                        for locs in self.LocationsList {
                            let sphereNode = self.sceneNode?.childNode(withName: String(index), recursively: true)
                            if locs.isEqual(trackInfo.coordinate){
                                SCNTransaction.begin()
                                
                                if let node = sphereNode?.geometry as? SCNBox  {
                                    
                                    let sphereMaterial = SCNMaterial()
                                    sphereMaterial.diffuse.contents = UIColor.green
                                    self.endNode = sphereNode
                                    node.materials = [sphereMaterial]
                                    
                                }
                                SCNTransaction.commit()
                                
                            }
                            
                            if  let childnodes = sphereNode?.childNodes  {
                                for textnode in childnodes {
                                    if let node2 = textnode.geometry as? SCNText {
                                        SCNTransaction.begin()
                                        //"Loading..."
                                        if  node2.string as? String  == "Loading..." {
                                            CardinalDirection.geoDecoding(self.LocationsList[index]) { (locname) in
                                                
                                                node2.string = locname ?? "Loading..."
                                                
                                            }
                                            
                                            //node2.string =  String(Double(round(100000*(BearAngle.distanceTwopoints(map1: curLoc.coordinate, map2: LocationsList[index])/1000))/100000) )
                                            
                                            
                                        }
                                    }
                                    SCNTransaction.commit()
                                    
                                    
                                    index = index + 1
                                    
                                }
                            }
                            
                        }
                    }
                    
                }
                
            }
        }
        
        
        
    }
    //MARK :- ADD navigation  nodes
    func addNaviationControlPoints(){
        let defaults = UserDefaults.standard
        let stepsData = defaults.object(forKey: "Steps") as? NSData
        let directionData = defaults.object(forKey: "Directions") as? NSData
        if let stepsData = stepsData ,let directionData = directionData {
            let stepsArray = NSKeyedUnarchiver.unarchiveObject(with: stepsData as Data) as? [CLLocation]
            let directionArray = NSKeyedUnarchiver.unarchiveObject(with:directionData as Data) as? [String]
            if let stepsArray = stepsArray,let dirArray = directionArray {
                guard let cur = LocationManager.sharedInstance.currentLocation else {
                    return
                }
                //scene.rootNode.constraints = [constraint]
                var index = 0
                directionNodes.removeAll()
                var intialNode : SCNNode? = nil
                var finalNode : SCNNode? = nil
                var startVector = SCNVector3()
                var endVector1 = SCNVector3()
                angleList.removeAll()
                for step in stepsArray {
                    neaestDestination = BearAngle.distanceTwopoints(map1: cur.coordinate, map2: stepsArray[0].coordinate)
                    smallDistanceAngle =  BearAngle.bearingBetween(startLocation:  cur.coordinate, endLocation: stepsArray[0].coordinate)
                    
                    debugPrint(step.coordinate)
                    angleList.append(BearAngle.bearingBetween(startLocation:  cur.coordinate, endLocation: step.coordinate))
                    //                    direction.sphere(currentLocation: cur.coordinate, endloc: step.coordinate,locname:String(index), isSteps: true, direction: dirArray[index])
                    let direction = CardinalDirection(name: "a", angle: 0.0)
                    //                    let node = DirectionNavNode("s" + String(index),step.coordinate, dirArray[index])
                    //                        //BaseNode("s" + String(index),step.coordinate, dirArray[index])
                    let node  = direction.addNavigationNode(currentLocation: cur.coordinate, endloc: step.coordinate, locname: String(index), isSteps: true, direction: dirArray[index])
                    directionNodes.append(node)
                    
                    sceneView.scene.rootNode.addChildNode(node)
                    
                    //                    let anchor = ARAnchor(transform:float4x4.init(node.transform))
                    //                    sceneView.session.add(anchor: anchor)
                    // debugPrint(finalNode)
                    if finalNode != nil {
                        
                        finalNode = node
                        let mat = SCNMaterial()
                        mat.diffuse.contents = UIColor.green
                        startVector = SCNVector3.positionFromTransform((intialNode?.worldTransform)!)
                        endVector1 = SCNVector3.positionFromTransform((finalNode?.worldTransform)!)
                        //debugPrint(startVector)
                        // debugPrint(endVector1)
                        let slope :Double =  Double((endVector1.z - startVector.z) / (endVector1.x - startVector.x))
                        let angle = atan(slope) * (180 / M_PI)
                        debugPrint("Angle :- \(angle)")
                        _ = Float((startVector.x *  endVector1.x)+(startVector.y *  endVector1.y)+(startVector.z *  endVector1.z))
                        _ = Float(sqrt(startVector.x  * startVector.x  + startVector.y  * startVector.y  + startVector.z * startVector.z ))
                        _ = Float(sqrt(endVector1.x  * endVector1.x  + endVector1.y  * endVector1.y  + endVector1.z * endVector1.z ))
                        let lineNode =  CylinderLine(parent: intialNode!,destNode:finalNode!, v1: startVector, v2: endVector1, radius: 0.2, radSegmentCount: 0, color: .green,startDirection: dirArray[index - 1],endDirection : dirArray[index])
                        self.sceneView.scene.rootNode.addChildNode(lineNode)
                        
                        
                    }
                    
                    intialNode = node
                    
                    finalNode = node
                    index = index + 1;
                }
                
            }
        }
    }
    func drawPathBetweenNodes(){
        var index = 0
        var startNode = directionNodes[index]
        var endNode1 = directionNodes[index]
        
        var startVector = SCNVector3()
        var endVector = SCNVector3() //SCNVector3.positionFromTransform(endNode.worldTransform)
        let mat = SCNMaterial()
        
        mat.diffuse.contents  = UIColor.cyan
        mat.specular.contents = UIColor.green
        while index < directionNodes.count
        {
            index = index + 1
            if index < directionNodes.count {
                endNode1 = directionNodes[index]
                startVector = SCNVector3.positionFromTransform((startNode.worldTransform))
                endVector = SCNVector3.positionFromTransform((endNode1.worldTransform))
                debugPrint(startVector)
                debugPrint(endVector)
                
                //  let lineNode = LineNode(v1: startVector, v2: endVector, material: [mat])
                //self.sceneView.scene.rootNode.addChildNode(lineNode)
                startNode = endNode1
            }
            
        }
        startVector = SCNVector3.positionFromTransform((directionNodes.last?.worldTransform)!)
        endVector = SCNVector3.positionFromTransform((endNode?.worldTransform)!)
        debugPrint(startVector)
        debugPrint(endVector)
        //let lineNode = LineNode(v1: startVector, v2: endVector, material: [mat])
        //self.sceneView.scene.rootNode.addChildNode(lineNode)
        
        
    }
    func didSetLocation(location: CLLocation?){
        if isaddeed {
            isaddeed = false
            updateLocation()
            addNaviationControlPoints()
        }
        else{
            trackLocation()
        }
        
        //trackPolyline()
    }
    
    func updateLocation(){
        let constraint = SCNLookAtConstraint(target: sceneView.pointOfView)
        
        // Keep the rotation on the horizon
        constraint.isGimbalLockEnabled = false
        
        // Slow the constraint down a bit
        constraint.influenceFactor = 0.01
        
        // Finally add the constraint to the node
        angleList.removeAll()
        let scene = SCNScene()
        //scene.rootNode.constraints = [constraint]
        var index = 0
        for direction1 in LocationsList {
            guard let loc = LocationManager.sharedInstance.currentLocation else {
                
                return
            }
            
            
            //            let direction = CardinalDirection(name: "a", angle: 0.0)
            //            let node = direction.sphere(currentLocation: loc.coordinate, endloc: LocationsList[index],locname:String(index), isSteps: false, direction: "")
            //            scene.rootNode.addChildNode(node)
            let baseNode = BaseNode(String(index), LocationsList[index],nil)
            scene.rootNode.addChildNode(baseNode)
            var anchor = baseNode.anchor
            
            sceneView.session.add(anchor: anchor!)
            
            index = index + 1;
        }
        trackLocation()
        sceneNode = scene.rootNode
        // sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        sceneView.scene = scene
        //sceneView.projectPoint()
    }
    func getCurrentcursorpos()-> SCNVector3?{
        guard let currPos = sceneView.pointOfView else {
            return nil
        }
        return sceneView.scene.rootNode.convertPosition(currPos.worldPosition, to: sceneNode)
    }
    func didUpdateHeading(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading){
        
        
        if newHeading.headingAccuracy < 0 {
            // debugPrint(newHeading.trueHeading)
            // addDirectionPlane(direction:newHeading.trueHeading)
        }
        else {
            //addDirectionPlane(direction:newHeading.magneticHeading)
            // debugPrint(newHeading.magneticHeading)
        }
        
        
    }
    func getAllMaterials(_ image:UIImage) -> [SCNMaterial]{
        let mat = SCNMaterial()
        mat.diffuse.contents = image
        //        let mat2 = SCNMaterial()
        //        mat2.diffuse.contents = image
        //        let mat3 = SCNMaterial()
        //        mat3.diffuse.contents = image
        //        let mat4 = SCNMaterial()
        //        mat4.diffuse.contents = image
        return [mat]
    }
    func addDirectionPlane(direction:CLLocationDirection){
        let arrowPlane = sceneView.pointOfView?.childNode(withName: "arrow", recursively: true)
        guard let _ = smallDistanceAngle else {
            return
        }
        guard let planes = arrowPlane?.geometry as? SCNPlane else {
            let plane = SCNPlane(width: 10, height: 10)
            
            //            plane.width = 10
            //            plane.length = 20
            
            //SCNBox(width: 5.0, height: 10.0, length: 10.0, chamferRadius: 0.0)//SCNPlane(width: 5, height: 5)
            //
            plane.materials = getAllMaterials(#imageLiteral(resourceName: "up"))
            //  plane.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "up")
            let smallDistanceQuardant  = Int(ceil(smallDistanceAngle! / 90.0))
            let currentQuartdant  = Int(ceil(direction / 90.0))
            
            var lowerRange =  (Int(smallDistanceAngle! - 5) % 360)...Int(smallDistanceAngle!)
            var upperRange = (Int(smallDistanceAngle! + 1))...(Int(smallDistanceAngle! + 5))
            
            
            let directionNode = SCNNode()
            
            directionNode.name = "arrow"
            directionNode.geometry = plane
            
            directionNode.transform = SCNMatrix4MakeRotation(-Float.pi / 3, 1.0, 0, 0)
            
            //            directionNode.position = getCurrentcursorpos()!
            directionNode.position = SCNVector3Make(0, -2, -30.0)
            
            sceneView.pointOfView?.addChildNode(directionNode)
            //            sceneNode?.addChildNode(directionNode)
            return
        }
        
        let smallDistanceQuardant  = Int(ceil(smallDistanceAngle! / 90.0))
        let currentQuartdant  = Int(ceil(direction / 90.0))
        var lowerRange = 0...1
        var upperRange = 0...1
        if (Int(smallDistanceAngle! - 5)) <= 360 && (Int(smallDistanceAngle! + 5)) >= 360 {
            // centerVal = 360
            lowerRange =  (Int(smallDistanceAngle! - 5) % 360)...360
            upperRange = (0)...(Int(smallDistanceAngle! + 5))
        }
            
        else { lowerRange =  (Int(smallDistanceAngle! - 5) % 360)...Int(smallDistanceAngle!) % 360
            upperRange = (Int(smallDistanceAngle! + 1)  % 360)...(Int(smallDistanceAngle! + 5) % 360)
        }
        
        if lowerRange.contains(Int(direction)) || upperRange.contains(Int(direction)) {
            //planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "up")
            animationPlane(arrowPlane!, 0)
            planes.materials = getAllMaterials(#imageLiteral(resourceName: "up"))
        }
        else if currentQuartdant == 4 {
            if smallDistanceQuardant == 1 || smallDistanceQuardant == 2{
                planes.materials = getAllMaterials(#imageLiteral(resourceName: "Forward"))
                animationPlane(arrowPlane!, 1)
                //planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
            }
            else if smallDistanceQuardant == 3 {
                planes.materials = getAllMaterials(#imageLiteral(resourceName: "back"))
                animationPlane(arrowPlane!, 2)
                // planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
            }
            else if Int(direction) < lowerRange.lowerBound {
                planes.materials = getAllMaterials(#imageLiteral(resourceName: "Forward"))
                animationPlane(arrowPlane!, 1)
                //planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
            }
            else if Int(direction) >  upperRange.upperBound {
                planes.materials = getAllMaterials(#imageLiteral(resourceName: "back"))
                animationPlane(arrowPlane!, 2)
                // planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
            }
        }
        else if currentQuartdant == 3 {
            if smallDistanceQuardant == 4 || smallDistanceQuardant == 1{
                planes.materials = getAllMaterials(#imageLiteral(resourceName: "Forward"))
                animationPlane(arrowPlane!, 1)
                //planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
            }
            else if smallDistanceQuardant == 2 {
                planes.materials = getAllMaterials(#imageLiteral(resourceName: "back"))
                animationPlane(arrowPlane!, 2)
                //planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
            } else if Int(direction) < lowerRange.lowerBound {
                planes.materials = getAllMaterials(#imageLiteral(resourceName: "Forward"))
                animationPlane(arrowPlane!, 1)
                // planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
            }
            else if Int(direction) >  upperRange.upperBound {
                planes.materials = getAllMaterials(#imageLiteral(resourceName: "back"))
                animationPlane(arrowPlane!, 2)
                // planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
            }
        }
        else if currentQuartdant == 2{
            if smallDistanceQuardant == 1 || smallDistanceQuardant == 4{
                planes.materials = getAllMaterials(#imageLiteral(resourceName: "back"))
                animationPlane(arrowPlane!, 2)
                // planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
            }
            else if smallDistanceQuardant == 3 {
                planes.materials = getAllMaterials(#imageLiteral(resourceName: "Forward"))
                animationPlane(arrowPlane!, 1)
                //planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
            }
            else if Int(direction) < lowerRange.lowerBound {
                planes.materials = getAllMaterials(#imageLiteral(resourceName: "Forward"))
                animationPlane(arrowPlane!, 1)
                //planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
            }
            else if Int(direction) >  upperRange.upperBound {
                planes.materials = getAllMaterials(#imageLiteral(resourceName: "back"))
                animationPlane(arrowPlane!, 2)
                // planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
            }
        }
        else if currentQuartdant == 1{
            if smallDistanceQuardant == 4 || smallDistanceQuardant == 3{
                planes.materials = getAllMaterials(#imageLiteral(resourceName: "back"))
                animationPlane(arrowPlane!, 2)
                // planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
            }
            else if smallDistanceQuardant == 2 {
                planes.materials = getAllMaterials(#imageLiteral(resourceName: "Forward"))
                animationPlane(arrowPlane!, 1)
                // planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
            }
            else if Int(direction) < lowerRange.lowerBound {
                planes.materials = getAllMaterials(#imageLiteral(resourceName: "Forward"))
                animationPlane(arrowPlane!, 1)
                // planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Forward")
            }
            else if Int(direction) >  upperRange.upperBound {
                planes.materials = getAllMaterials(#imageLiteral(resourceName: "back"))
                animationPlane(arrowPlane!, 2)
                // planes.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "back")
            }
        }
        
        
    }
    
    /// direction 0 -> down , 1-> left , 2-> right
    func animationPlane(_ node:SCNNode, _ direction : Int) {
        node.removeAnimation(forKey: "extrude")
        node.removeAnimation(forKey:  "extrude1")
        node.removeAnimation(forKey: "extrude2")
        switch  direction {
        case 0:
            
            let animation = CABasicAnimation(keyPath: "position")
            animation.fromValue = SCNVector3Make(0, 0, -50.0)
            animation.toValue = SCNVector3Make(0, 0, -150.0)
            animation.duration = 1.0
            // animation.autoreverses = true
            animation.repeatCount = .infinity
            node.addAnimation(animation, forKey: "extrude")
        case 1:
            let animation = CABasicAnimation(keyPath: "position")
            animation.fromValue = SCNVector3Make(0, 0, -50.0)
            animation.toValue = SCNVector3Make(10, 0, -50.0)
            animation.duration = 1.0
            // animation.autoreverses = true
            animation.repeatCount = .infinity
            node.addAnimation(animation, forKey: "extrude1")
        case 2: 
            let animation = CABasicAnimation(keyPath: "position")
            animation.fromValue = SCNVector3Make(0, 0, -50.0)
            animation.toValue = SCNVector3Make(-10, 0, -50.0)
            animation.duration = 1.0
            // animation.autoreverses = true
            animation.repeatCount = .infinity
            node.addAnimation(animation, forKey: "extrude2")
        default:
            break
        }
        
    }
    // MARK: - ARSCNViewDelegate
    
    public func renderer(_ renderer: SCNSceneRenderer, didRenderScene scene: SCNScene, atTime time: TimeInterval) {
        
        //updateLocation()
    }
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        //        let material = SCNMaterial()
        //        material.diffuse.contents = UIColor.brown
        //         let mat = SCNBox(width: 1.0, height: 5, length: 3.0, chamferRadius: 0.0)
        //        mat.materials = [material]
        //        let node = SCNNode(geometry: mat)
        //
        //        node.transform = SCNMatrix4.init(anchor.transform)// anchor.transform
        
        
        
        // updateLocation()
        return nil
    }
    
    public func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor){
        //        let material = SCNMaterial()
        //        material.diffuse.contents = UIColor.brown
        //        let mat = SCNBox(width: 1.0, height: 5, length: 3.0, chamferRadius: 0.0)
        //        mat.materials = [material]
        //        node.geometry = mat
        //
        //        node.transform = SCNMatrix4.init(anchor.transform)// anchor.transform
    }
    public func renderer(_ renderer: SCNSceneRenderer, willUpdate node: SCNNode, for anchor: ARAnchor){
        
    }
    
    public func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor)
    {
        
    }
    public func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        
    }
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    func session(_: ARSession, didAdd: [ARAnchor]) {
        for anchor in didAdd {
            
        }
    }
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        sceneView.session.run(session.configuration!,
                              options: [.resetTracking,
                                        .removeExistingAnchors])
    }
    
    
    @IBAction func addLocations(_ sender: UIButton) {
        sceneView.scene.rootNode.enumerateChildNodes { (node, stop) -> Void in
            node.removeFromParentNode()
        }
        LocationsList.removeAll()
        smallDistanceAngle = nil
        neaestDestination = nil
        isaddeed = true
        endNode = nil
        lastPath = nil
        //let add = ADDLocationVC(nibName: "ADDLocationVC", bundle: nil)
        DispatchQueue.main.async {
            let add = GoogleMapsVC(nibName: "GoogleMapsVC", bundle: nil)
            add.stepsHadlerCompletion = { (steps) in
                self.stepsList = steps
                //  _ = self.trackPolyline()
            }
            self.navigationController?.pushViewController(add, animated: true)
        }
        
    }
    
    @IBAction func clearNodes(_ sender: Any) {
        self.directionsLabel.text = ""
        self.directionNodes.removeAll()
        guard let _  = sceneNode else {
            return
        }
        lastPath = nil
        sceneView.session.pause()
        sceneView.scene.rootNode.enumerateChildNodes { (node, stop) -> Void in
            node.removeFromParentNode()
        }
        smallDistanceAngle = nil
        neaestDestination = nil
        isaddeed = true
        guard let oldConf = sceneView.session.configuration else {
            return
        }
        sceneView.session.run(oldConf)
        let defaults = UserDefaults.standard
        //let placesData1 = NSKeyedArchiver.archivedData(withRootObject: [])
        defaults.set(nil, forKey: "Places")
        defaults.set(nil, forKey: "TrackLocation")
        defaults.set(nil, forKey: "Steps")
        defaults.set(nil, forKey: "Directions")
        LocationsList.removeAll()
        UserDefaults.standard.synchronize()
        updateLocation()
    }
}
extension ViewController {
    class func geoDecoding(_ geoLoc:CLLocationCoordinate2D,completionHandler handler:@escaping completeHandler) {
        let location = CLLocation(latitude: geoLoc.latitude, longitude: geoLoc.longitude)
        CLGeocoder().reverseGeocodeLocation(location) { (placeMark, error) in
            guard let places  = placeMark else {
                return
            }
            if error != nil {
                //  print(error)
                return
            }
            else if places.count > 0 {
                let pm = places[0]
                var subname = ""
                var local = ""
                if let sub = pm.subLocality {
                    subname = sub
                }
                if let local1 = pm.locality {
                    local = local1
                }
                
                handler(subname + " " + local)
                
            }
        }
        
    }
    func pushTransition(label: UILabel, animationType subType: String, animationDuration duration: CGFloat) {
        
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        animation.repeatCount = HUGE
        animation.autoreverses = true
        animation.type = kCATransitionPush
        animation.subtype = subType
        animation.duration = CFTimeInterval(duration)
        label.layer.add(animation, forKey: "kCATransitionPush")
        
    }
}


extension ViewController : SCNSceneRendererDelegate{
    public func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval){
        //        updateLocation()
        //trackLocation()
        //        SCNTransaction.commit()
    }
    public func renderer(_ renderer: SCNSceneRenderer, didApplyAnimationsAtTime time: TimeInterval){
        
    }
    public func renderer(_ renderer: SCNSceneRenderer, didApplyConstraintsAtTime time: TimeInterval){
        
    }
    
    func addPath(intialNode : SCNNode,startPoint :CGPoint,endPoint :CGPoint){
        // Material colors
        let cyanMaterial = SCNMaterial()
        cyanMaterial.diffuse.contents = UIColor.cyan
        
        let anOrangeMaterial = SCNMaterial()
        anOrangeMaterial.diffuse.contents = UIColor.orange
        
        let aPurpleMaterial = SCNMaterial()
        aPurpleMaterial.diffuse.contents = UIColor.purple
        
        // A bezier path-0.25
        
        let bezierPath = UIBezierPath()
        //        bezierPath.move(to: startPoint)
        //       bezierPath.addCurve(to: endPoint, controlPoint1: <#T##CGPoint#>, controlPoint2: <#T##CGPoint#>)
        //        bezierPath.close()
        
        // Add shape
        let shape = SCNShape(path: bezierPath, extrusionDepth: 0.75)
        shape.materials = [cyanMaterial, anOrangeMaterial, aPurpleMaterial]
        let shapeNode = SCNNode(geometry: shape)
        shapeNode.position = intialNode.position
        //        shapeNode.position = SCNVector3(x: 0.2, y: 0.75, z: 0.1);
        sceneView.scene.rootNode.addChildNode(shapeNode)
        shapeNode.rotation = SCNVector4(x: -1.0, y: -1.0, z: 0.0, w: 0.0)
    }
    
    
}
extension ViewController {
    func trackPolyline()  {
        
        //store the last step user where he is
        for step in stepsList {
            //get polyline object
            let poly = GMSPolyline()
            poly.path = GMSPath(fromEncodedPath: step.points)
            guard let current  = LocationManager.sharedInstance.currentLocation else {
                return
            }
            if GMSGeometryIsLocationOnPathTolerance(current.coordinate, poly.path!, true, 10){
                //
                //check for Start node exist and update it's location based on the current step and value
                if let startNode = checkForNodeExist(nodeIdentfier: "a0a", pointLocation: step.startlocation.coordinate, dir: step.direction,path: (poly.path?.encodedPath())! ),let endNode = checkForNodeExist(nodeIdentfier: "a1a", pointLocation: step.endLocation.coordinate, dir: step.nextDirection ?? "South",path: (poly.path?.encodedPath())!) {
                    let startVector = SCNVector3.positionFromTransform((startNode.worldTransform))
                    let endVector = SCNVector3.positionFromTransform((endNode.worldTransform))
                    if let lineNode = self.sceneView.scene.rootNode.childNode(withName: "LineNode", recursively: true) as? CylinderLine{
                        if  lastPath == nil || lastPath?.encodedPath() != poly.path?.encodedPath() {
                            
                            lastPath = poly.path
                            lineNode.updateNode(destNode: endNode, startVector: startVector, endVector: endVector, startDirection:"Head", endDirection: step.nextDirection ?? "Head")
                        }
                        
                    }
                    else {
                        let lineNode =  CylinderLine(parent: startNode,destNode : endNode ,v1: startVector, v2: endVector, radius: 0.2, radSegmentCount: 0, color: .green, startDirection:step.direction ,endDirection : step.nextDirection ?? "Head")
                        self.sceneView.scene.rootNode.addChildNode(lineNode)
                    }
                    
                }
                //  }
                
            }
        }
        
        
        
    }
    func checkForNodeExist(nodeIdentfier : String,pointLocation : CLLocationCoordinate2D,dir: String,path : String) -> SCNNode? {
        //check for Start node exist and update it's location based on the current step and value
        guard let curent = LocationManager.sharedInstance.currentLocation  else {
            return nil
        }
        if let startNode = sceneNode?.childNode(withName: nodeIdentfier, recursively: true)  {
            //update the node postion
            if  lastPath == nil || lastPath?.encodedPath() != path {
                
                lastPath = GMSPath(fromEncodedPath: path)
                startNode.transform = CardinalDirection.getTransformGiven(currentLocation: curent.coordinate, endLocation: pointLocation, isSteps: true)
            }
            
            return startNode
            //update the transform
            
        }
        else{
            //set up the start node and
            let direction = CardinalDirection(name: "a", angle: 0.0)
            let node = direction.addNavigationNode(currentLocation: curent.coordinate, endloc: pointLocation, locname: nodeIdentfier, isSteps: true, direction: dir)
            self.sceneView.scene.rootNode.addChildNode(node)
            return node
            
        }
    }
    func getNodes()->[SCNNode]{
        var index  = 0
        var controlNodesList : [SCNNode] = [SCNNode]()
        while index < 4 {
            if let node = self.sceneNode?.childNode(withName: "s+\(index)", recursively: true) {
                controlNodesList.append(node)
            }
            index = index + 1
        }
        return controlNodesList
    }
    
}
extension UIViewController {
    func showAlert(_ alertString : String){
        let alert = UIAlertController(title: "ARLocationTracker", message:alertString,  preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler : nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}
